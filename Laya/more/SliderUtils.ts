module more {

	/**
	 * 子图配置
	 */
	interface ISheetFrame {
		x: number;			// 裁剪坐标
		y: number;			// 裁剪坐标
		w: number;			// 裁剪宽
		h: number;			// 裁剪高
		offX: number;		// 裁剪偏移
		offY: number;		// 裁剪偏移
		sourceW: number;	// 实际宽
		sourceH: number;	// 实际高
	}

	/**
	 * 合图配置
	 */
	interface ISheetInfo {
		file: string;	// 合图相对路径
		frames: { [key: string]: ISheetFrame };	// 子图配置
	}

	/**
	 * 滑块列表工具
	 */
	export class SliderUtils {

		/**
		 * 合图地址
		 */
		public static sheetUrl: string = 'https://static.xunguanggame.com/moreGame/sliderbar.json';

		/**
		 * 合图纹理
		 */
		private static $textures: { [key: string]: Laya.Texture };

		/**
		 * 初始化文件
		 */
		protected static async initFile(): Promise<any> {
			var self = SliderUtils;
			var url = self.sheetUrl;
			var data = <ISheetInfo>await MoreUtils.getResByUrl(url);
			var png = url.substr(0, url.lastIndexOf('/') + 1) + data.file;
			var sheet = <Laya.Texture>await MoreUtils.getResByUrl(png);
			var frames = data.frames;
			var create = Laya.Texture.create;
			var textures = self.$textures = {};
			for (let i in frames) {
				let frame = frames[i];
				textures[i] = create(sheet, frame.x, frame.y, frame.w, frame.h,
					frame.offX, frame.offY, frame.sourceW, frame.sourceH);
			}
		}

		/**
		 * 设置图片
		 * @param img 图片
		 * @param name 合图里的子图别称，如'xxx_png'
		 */
		public static setImage(img: Laya.Image, name: string): void {
			img.source = SliderUtils.$textures[name];
		}

		/**
		 * 创建滑块，默认贴着父控件左边，Y值可修改
		 * @param parent 滑块存放位置，建议高度等同于屏幕高
		 * @param y 滑块的Y值，不传则默认0.2624位置
		 */
		public static async createSliderBar(parent: Laya.Sprite, y?: number): Promise<void> {
			var self = SliderUtils;
			await self.initFile();
			if (y == void 0)
				y = parent.height * 0.2624;
			var slide = new Laya.Image;
			slide.x = -62;
			slide.y = y;
			parent.addChild(slide);
			self.setImage(slide, 'icon_sidebar_png');
			// 监听
			var stage = Laya.stage;
			var enable = function (bool) {
				stage.mouseEnabled = bool;
			};
			// 点击滑块
			MoreUtils.addClickListener(slide, function () {
				let tween = Tween;
				// 添加列表
				let list = new SliderList;
				let time = 120;
				list.x = -750;
				parent.addChild(list);
				// 出场
				enable(false);
				tween.get(slide).to({ x: -154 }, time);
				tween.get(list).to({ x: 0 }, time).call(enable, null, true);
				// 关闭
				list.addClose(function () {
					enable(false);
					tween.get(slide).to({ x: -62 }, time);
					tween.get(list).to({ x: -750 }, time).call(function () {
						enable(true);
						parent.removeChild(list);
					});
				});
			});
		}
	}
}