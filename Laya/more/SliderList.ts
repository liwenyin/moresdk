module more {

	/**
	 * 滑块列表
	 */
	export class SliderList extends Laya.View {

		private m_rtBg: Laya.Sprite;
		private m_imgClose: Laya.Image;
		private m_ltGame0: Laya.List;
		private m_ltGame1: Laya.List;

		public constructor() {
			super();
			var width = this.width = 750;
			var height = this.height = Laya.stage.height;
			this.once(Laya.Event.UNDISPLAY, this, this.onDestroy);
			// 透明背景
			var rect = this.m_rtBg = new Laya.Sprite;
			rect.alpha = .4;
			rect.width = width;
			rect.height = height;
			rect.graphics.drawRect(0, 0, width, height, '#0');
			this.addChild(rect);
			// 内容
			var group = new Laya.Component;
			group.width = 666;
			group.height = 1178;
			group.centerX = group.centerY = 0;
			this.addChild(group);
			// 背景
			var set = SliderUtils.setImage;
			var image = new Laya.Image;
			image.centerX = 0;
			image.height = 1020;
			image.sizeGrid = '120,0,90,0';
			set(image, 'pic_popbg_sidebar_png');
			group.addChild(image);
			// 关闭按钮
			var image = this.m_imgClose = new Laya.Image;
			set(image, 'btn_close_sidebar_png');
			image.x = 608;
			image.y = 54;
			image.anchorX = image.anchorY = .5;
			group.addChild(image);
			// 底图
			var image = new Laya.Image;
			set(image, 'pic_base01_sidebar_png');
			image.y = 120;
			image.centerX = 0;
			image.width = 604;
			image.height = 540;
			image.sizeGrid = '26,28,26,28';
			group.addChild(image);
			// 列表0
			var list = this.m_ltGame0 = new Laya.List;
			list.width = 560;
			list.height = 494;
			list.centerX = 0;
			list.y = 144;
			list.repeatX = 1;
			list.spaceY = 12;
			group.addChild(list);
			// 
			var image = new Laya.Image;
			set(image, 'pic_base02_sidebar_png');
			image.centerX = 0;
			image.y = 676;
			image.width = 604;
			image.sizeGrid = '0,32,0,200';
			group.addChild(image);
			// 小猪
			var image = new Laya.Image;
			set(image, 'pic_pig_sidebar_png');
			image.y = 907;
			group.addChild(image);
			// 列表1
			var list = this.m_ltGame1 = new Laya.List;
			list.width = 495;
			list.height = 260;
			list.x = 134;
			list.y = 684;
			list.spaceX = list.spaceY = 5;
			list.repeatX = 4;
			group.addChild(list);
			// 其他
			this.initList();
		}

		/**
		 * 初始化列表
		 */
		protected initList(): void {
			var self = this;
			var list0 = self.m_ltGame0;
			var list1 = self.m_ltGame1;
			var config = MoreUtils.config;
			list0.itemRender = SliderItem0;
			list1.itemRender = SliderItem1;
			list0.array = config.list0;
			list1.array = config.list1;
		}

		protected onDestroy(): void {
			var self = this;
			var utils = MoreUtils;
			self.m_ltGame0.array = self.m_ltGame1.array = null;
			self.offAll();
		}

		/**
		 * 添加关闭回调
		 */
		public addClose(call: Function, thisObj?: any): void {
			var self = this;
			var clzz = MoreUtils;
			clzz.addClickListener(self.m_rtBg, call, thisObj);
			clzz.addClickScaleListener(self.m_imgClose, call, thisObj);
		}
	}
}