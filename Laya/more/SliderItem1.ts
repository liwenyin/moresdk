module more {

	/**
	 * 子项数据
	 */
	interface ISliderData1 extends IMiniConfig {
		reddot: boolean;		// 显示红点
	}

	/**
	 * 滑块列表子项1
	 */
	export class SliderItem1 extends Laya.Sprite {

		private m_imgLogo: Laya.Image;
		private m_imgDot: Laya.Image;

		private _dataSource: ISliderData1;

		public constructor() {
			super();
			var set = SliderUtils.setImage;
			this.width = this.height = 120;
			// logo
			var image = this.m_imgLogo = new Laya.Image;
			image.y = 10;
			image.width = image.height = 110;
			this.addChild(image);
			// 遮罩
			var image = new Laya.Image;
			set(image, 'game_mask_png');
			image.y = 10;
			image.width = image.height = 110;
			this.addChild(image);
			// 红点
			var image = this.m_imgDot = new Laya.Image;
			set(image, 'pic_new_sidebar_png');
			image.x = 96;
			this.addChild(image);
			// 监听
			MoreUtils.addClickListener(this, this.onClick, this);
		}

		/**
		 * 自定义设置数据
		 */
		public set dataSource(data: ISliderData1) {
			var self = this;
			self._dataSource = data;
			self.update();
		}

		/**
		 * 点击
		 */
		protected onClick(): void {
			var config = this._dataSource;
			MoreUtils.toMiniProgram(config.appId, config.path, config.preImg);
		}

		/**
		 * 刷新界面
		 */
		public update(): void {
			var self = this;
			var data = self._dataSource;
			if (data) {
				self.m_imgLogo.skin = data.logo;
				self.m_imgDot.visible = data.reddot;
			}
		}
	}
}