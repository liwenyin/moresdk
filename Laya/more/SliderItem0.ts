module more {

	/**
	 * 子项数据
	 */
	interface ISliderData0 extends IMiniConfig {
		number: number;		// 钻石数量
	}

	/**
	 * 滑块列表子项0
	 */
	export class SliderItem0 extends Laya.Sprite {

		private m_imgLogo: Laya.Image;
		private m_lblName: Laya.Label;
		private m_lblNum: Laya.Label;
		private m_imgRec: Laya.Image;

		private _dataSource: ISliderData0;

		public constructor() {
			super();
			var width = this.width = 560;
			this.height = 134;
			var set = SliderUtils.setImage;
			var image = new Laya.Image;
			set(image, 'pic_grade_sidebar_png');
			image.sizeGrid = '60,75,60,75';
			image.width = width;
			this.addChild(image);
			// logo
			var image = this.m_imgLogo = new Laya.Image;
			image.x = 20;
			image.y = 12;
			image.width = image.height = 110;
			this.addChild(image);
			// 遮罩
			var image = new Laya.Image;
			set(image, 'game_mask_png');
			image.x = 20;
			image.y = 12;
			image.width = image.height = 110;
			this.addChild(image);
			// 名称
			var label = this.m_lblName = new Laya.Label;
			label.x = 162;
			label.y = 30;
			label.fontSize = 28;
			label.color = '#261009';
			this.addChild(label);
			// 
			var image = new Laya.Image;
			set(image, 'pic_count_sidebar_png');
			image.x = 156;
			image.y = 68;
			this.addChild(image);
			// 数量
			var label = this.m_lblNum = new Laya.Label;
			label.centerX = -30;
			label.centerY = 23;
			label.fontSize = 28;
			label.color = '#a74f07';
			this.addChild(label);
			// 按钮
			var image = this.m_imgRec = new Laya.Image;
			set(image, 'btn_get_sidebar_png');
			image.centerX = 173;
			image.centerY = 0;
			this.addChild(image);
			MoreUtils.addClickScaleListener(image, this.onClick, this);
		}

		/**
		 * 界面刷新，可手动调用强制刷新
		 */
		public update(): void {
			var self = this;
			var data = self._dataSource;
			if (data) {
				self.m_imgLogo.skin = data.logo;
				self.m_lblName.text = data.name;
				self.m_lblNum.text = (data.number || 500) + '';
			}
		}

		/**
		 * 自定义设置数据
		 */
		public set dataSource(data: ISliderData0) {
			var self = this;
			self._dataSource = data;
			self.update();
		}

		/**
		 * 点击
		 */
		protected onClick(): void {
			var config = this._dataSource;
			MoreUtils.toMiniProgram(config.appId, config.path, config.preImg);
		}
	}
}