module more {

	declare var wx;

	/**
	 * 小程序配置
	 */
	export interface IMiniConfig {
		logo: string;		// 显示的图标
		name: string;		// 游戏名称
		appId: string;		// 游戏ID
		path: string;		// 跳转路径
		preImg: string;		// 无法跳转时弹出图片
		size: { w: number, h: number };	// 设置图标尺寸
	}

	/**
	 * 更多游戏配置
	 */
	export interface IMoreConfig {
		games: IMiniConfig[];	// logo
		list0: IMiniConfig[];	// 列表0
		list1: IMiniConfig[];	// 列表1
		banner: IMiniConfig[];	// 底部
		pollTime: number;		// 轮询时间，默认30秒
		updateTime: number;		// 刷新配置时间
	}

	/**
	 * 监听对象
	 */
	interface TargetEvent {
		type: string;
		listener: Function;
		thisObject: any;
		useCapture: boolean;
		priority: number;
	}

	interface IScaleTarget extends Laya.Sprite {
		$hashBegin?: boolean;// 是否按下
		$bgScaleX?: number;	// 初始缩放值
		$bgScaleY?: number;
		$evtScale?: number;	// 缩放倍数
	}

	var LEvent = Laya.Event;
	var Handler = Laya.Handler;

	/**
	 * 更多游戏工具类，微信交接工具
	 */
	export class MoreUtils {

		/**
		 * 配置s
		 */
		public static config: IMoreConfig;

		/**
		 * 循环左右摇晃动画，注意控件必须锚点居中或者有水平垂直约束，否则会很怪异
		 * @param time 第一段动画时间
		 */
		public static swing(target: Laya.Sprite, time: number = 100): void {
			Tween.get(target, { loop: true }).to({
				rotation: 30
			}, time).to({
				rotation: -30
			}, time * 1.6).to({
				rotation: 15
			}, time * 1.2).to({
				rotation: -15
			}, time * .8).to({
				rotation: 0
			}, time * .6).wait(time * 8);
		}

		//// 监听相关 ////

		/**
		 * 添加点击按下监听
		 */
		public static addTouchBeginListener(target: Laya.EventDispatcher, call: Function, thisObj?: any): void {
			target.on(LEvent.MOUSE_DOWN, thisObj, call);
		}

		/**
		 * 添加监听结束监听
		 */
		public static addTouchFinishListener(target: Laya.EventDispatcher, finish: Function, thisObj?: any): void {
			target.on(LEvent.MOUSE_UP, thisObj, finish);
			target.on(LEvent.MOUSE_OUT, thisObj, finish);
		}

		/**
		 * 添加TouchTap监听
		 */
		public static addClickListener(target: Laya.Sprite, call: Function, thisObj?: any, useCapture?: boolean): void {
			target.on(LEvent.CLICK, thisObj, call);
		}

		/**
		 * 添加缩放监听，记得用removeEventListener来移除这个监听
		 */
		public static addScaleListener(target: IScaleTarget, scale: number = 0.95): void {
			var self = MoreUtils;
			target.$evtScale = scale;
			target.$bgScaleX = target.scaleX;
			target.$bgScaleY = target.scaleY;
			self.addTouchBeginListener(target, self.onScaleBegin, self);
			self.addTouchFinishListener(target, self.onScaleEnd, self);
		}

		/**
		 * 缩放开始
		 */
		protected static onScaleBegin(event: Laya.Event): void {
			var target = <IScaleTarget>event.currentTarget;
			var tween = Tween;
			var scale = target.$evtScale;
			var scaleX = target.scaleX = target.$bgScaleX;
			var scaleY = target.scaleY = target.$bgScaleY;
			scaleX *= scale;
			scaleY *= scale;
			target.$hashBegin = true;
			tween.get(target).to({ scaleX: scaleX, scaleY: scaleY }, 100);
		}

		/**
		 * 缩放结束
		 */
		protected static onScaleEnd(event: Laya.Event): void {
			var target = <IScaleTarget>event.currentTarget;
			if (target.$hashBegin) {
				let time = 100;
				let scaleX = target.$bgScaleX;
				let scaleY = target.$bgScaleY;
				let bScaleX = scaleX * 1.1;
				let bScaleY = scaleY * 1.1;
				Tween.get(target).to({ scaleX: bScaleX, scaleY: bScaleY }, time).
					to({ scaleX: scaleX, scaleY: scaleY }, time);
				target.$hashBegin = void 0;
			}
		}

		/**
		 * 在click的基础上进行缩放
		 */
		public static addClickScaleListener(target: IScaleTarget, call: Function, thisObj?: any, scale?: number): void {
			var self = MoreUtils;
			self.addScaleListener(target, scale);
			self.addClickListener(target, call, thisObj);
		}

		//// 监听结束 ////

		/**
		 * 跳转到小程序
		 * @param appId 小程序的appId
		 * @param path 跳转路径
		 * @param extraData 扩展消息
		 * @param preImg 低版本弹出图片
		 */
		public static toMiniProgram(appId: string, path?: string, preImg?: string): Promise<boolean> {
			var toMini = wx.navigateToMiniProgram;
			if (toMini) {
				return new Promise<boolean>(function (resolve) {
					toMini({
						appId: appId,
						path: path,
						envVersion: 'trial',
						// extraData: extraData,
						success: function (res) {
							resolve(true);
						},
						fail: function (res) {
							MoreUtils.previewImage(preImg).then(resolve);
						}
					});
				});
			}
			else
				return MoreUtils.previewImage(preImg);
		}

		/**
		 * 显示图片
		 */
		public static previewImage(url: string): Promise<boolean> {
			return new Promise<boolean>(function (resolve) {
				url ? wx.previewImage({
					urls: [url],
					success: function () {
						resolve(true);
					},
					fail: function () {
						resolve(false);
					}
				}) : resolve(false);
			})
		}

		/**
		 * 检测文件格式
		 * @param url 文件地址
		 * @param format 格式，建议首字符为'.'，如'.png'、'.json'等
		 */
		public static checkFile(url: string, format: string): boolean {
			return url.indexOf(format) == url.length - format.length;
		}

		/**
		 * 将source的内容全部插入target
		 * @param target 目标
		 * @param source 来源
		 * @param inHead 是否插在头部
		 */
		public static insert<T>(target: T[], source: T[], inHead?: boolean): void {
			target[inHead ? 'unshift' : 'push'].apply(target, source);
		}

		/**
		 * 是否有值
		 */
		public static hasValue(obj: any): boolean {
			// 右侧兼容空字符串和0
			return !!obj || (obj != null && obj != void 0);
		}

		/**
		 * 根据网络地址加载资源
		 * @param url  
		 */
		public static getResByUrl(url: string): Promise<any> {
			return new Promise<any>(function (resolve) {
				let loader = Laya.loader;
				loader.load(url, Handler.create(null, function () {
					resolve(loader.getRes(url));
				}));
			});
		}

		/**
		 * 设置SDK配置，需要优先调用
		 */
		public static setConfig(url: string): Promise<IMoreConfig> {
			var self = MoreUtils;
			var repeate = new RepeatUtils(1000, 5);
			var call = function () {
				return new Promise<boolean>(function (resolve) {
					self.getResByUrl(url).then(function (data: IMoreConfig) {
						// 刷新配置操作
						let uTime = data.updateTime;
						uTime > 0 && setTimeout(self.setConfig, self, uTime, url);
						// 返回
						self.config = data;
						resolve(true);
					}).catch(function () {
						resolve(false);
					});
				});
			};
			repeate.setRepeatCall(call);
			return new Promise<IMoreConfig>(function (resolve, reject) {
				repeate.setFinishCall(function (bool: boolean) {
					bool ? resolve(self.config) : reject();
				});
			});
		}
	}
}