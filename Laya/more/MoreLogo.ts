module more {

	/**
	 * 更多游戏——单个logo
	 * 注：该logo的锚点位于中心点，因此加入场景时注意坐标
	 */
	export class MoreLogo extends ClipImage {

		private $initAc: boolean;
		private $index: number;
		private $updateInt: number;
		private $games: IMiniConfig[];

		/**
		 * @param type 图标类型，默认games配置
		 */
		public constructor(type?: number) {
			super();
			this.anchorX = this.anchorY = .5;
			this.initView(MoreUtils.config, type)
		}

		/**
		 * 重写
		 */
		protected onChange(): void {
			var self = this;
			var source = self.source;
			if (!self.$initAc) {
				let width = 0, height = 0;
				if (source) {
					width = source.width;
					height = source.height;
				}
				self.width = width;
				self.height = height;
			}
		}

		protected onDestroy(): void {
			this.clear();
		}

		/**
		 * 初始化界面
		 */
		protected initView(config: IMoreConfig, type?: number): void {
			var self = this;
			// 显示Logo
			var isBanner = type == 1;
			var games = self.$games = config[isBanner ? 'banner' : 'games'];
			var length = games && games.length;
			if (length > 0) {
				let clzz = MoreUtils;
				// 先刷一次
				self.$index = -1;
				self.updateIndex();
				// 轮询
				if (length > 1) {
					let pollTime = config.pollTime;
					if (!(pollTime > 0))
						pollTime = 30000;
					self.$updateInt = more.setInterval(self.updateIndex, self, pollTime);
				}
				// 其他
				isBanner || clzz.swing(self);
				clzz.addClickListener(self, self.onClick, self);
			}
			else
				self.skin = '';
		}

		/**
		 * 更新logo下标
		 */
		protected updateIndex(): void {
			var self = this;
			var games = self.$games;
			var index = self.$index = (self.$index + 1) % games.length;
			var game = games[index], logo = game.logo;
			// 初始化
			self.$initAc = false;
			self.clearMovie();
			// 尺寸
			var size = game.size;
			if (size) {
				self.width = size.w;
				self.height = size.h;
				self.$initAc = true;
			}
			else {
				self.width = self.height = void 0;
			}
			// 动画
			if (MoreUtils.checkFile(logo, '.json'))
				self.initMovie(logo.substr(0, logo.length - 5));
			else
				self.skin = logo;
		}

		/**
		 * 初始化动画
		 */
		private async initMovie(name: string): Promise<void> {
			var self = this;
			var getRes = MoreUtils.getResByUrl;
			var pngUrl = name + '.png';
			var frame = await getRes(name + '.json');
			var sheet = await getRes(pngUrl);
			self.setClip(frame, sheet);
			self.play();
		}

		/**
		 * 清除动画定时器
		 */
		private clearMovie(): void {
			var self = this;
			self.stop();
			self.skin = '';
		}

		/**
		 * 点击
		 */
		protected onClick(): void {
			var config = this.$games[this.$index];
			config && MoreUtils.toMiniProgram(
				config.appId, config.path, config.preImg);
		}

		/**
		 * 清除事件、动画
		 */
		public clear(): void {
			var self = this;
			self.skin = '';
			Tween.clear(self);
			self.offAll(Laya.Event.CLICK);
			clearInterval(self.$updateInt);
		}
	}
}