module more {

	/**
	 * 切片帧数据
	 */
	interface IClipFrame {
		res: string;
		x: number;
		y: number;
	}

	/**
	 * 切片动画数据
	 */
	interface IClipAni {
		frameRate: number;
		frames: IClipFrame[];
	}

	/**
	 * 切片资源数据
	 */
	interface IClipRes {
		x: number;
		y: number;
		w: number;
		h: number;
	}

	/**
	 * 切片数据
	 */
	interface IClipData {
		mc: { [key: string]: IClipAni };
		res: { [key: string]: IClipRes };
	}

	/**
	 * 切片图，帧动画
	 */
	export class ClipImage extends Laya.Image {

		private $mcData: { [key: string]: IClipAni };
		private $textures: { [key: string]: Laya.Texture };
		private $interval: number;

		public constructor(skin?: string) {
			super(skin);
			this.on(Laya.Event.LOADED, this, this.onChange);
			this.once(Laya.Event.UNDISPLAY, this, this.$unDisplay);
		}

		/**
		 * 离开舞台调用，重写前需注意不要删除原有功能
		 */
		protected $unDisplay(): void {
			var self = this;
			self.$mcData = self.$textures = null;
			self.stop();
			self.offAll();
			self.onDestroy();
		}

		/**
		 * 修改纹理调用
		 */
		protected onChange(): void {

		}

		/**
		 * 离开舞台调用
		 */
		protected onDestroy(): void {

		}

		/**
		 * 设置切片
		 * @param data 帧数据
		 * @param sheet 合图
		 */
		public setClip(data: IClipData, sheet: Laya.Texture): void {
			var self = this;
			// 创建子图
			var res = data.res;
			var textures = self.$textures = {};
			var createTxt = Laya.Texture.createFromTexture;
			for (let i in res) {
				let item = res[i];
				textures[i] = createTxt(sheet, item.x, item.y, item.w, item.h);
			}
			self.$mcData = data.mc;
		}

		/**
		 * 播放
		 * @param aniName 动画名称，为空则选择首个动画
		 */
		public play(aniName?: string): void {
			var self = this;
			var mcData = self.$mcData, textures = self.$textures;
			if (mcData && textures) {
				aniName || (aniName = Object.keys(mcData)[0]);
				// 动画开始
				let frame = aniName && mcData[aniName];
				if (frame) {
					let index = 0;
					let speed = 1000 / (frame.frameRate || 5);
					let frames = frame.frames;
					let frmLen = frames.length;
					let call = function () {
						self.source = textures[frames[index++].res];
						index %= frmLen;
					};
					call();	// 先执行一次
					self.$interval = setInterval(call, null, speed);
				}
			}
		}

		/**
		 * 停止播放
		 */
		public stop(): void {
			var self = this;
			if (self.$interval) {
				clearInterval(self.$interval);
				self.$interval = null;
			}
		}
	}
}