// SDK示例：设置配置，若存在，则执行SDK界面绘制
more.MoreUtils.setConfig('https://static.xunguanggame.com/moreGame/sdks/model.json').then(function (config) {
	var stage = Laya.stage;
	// 单个图标
	var logo = new more.MoreLogo();
	stage.addChild(logo);
	logo.x = logo.y = 100;
	// 左边滑块-列表示例
	var main = new Laya.Sprite;
	main.width = stage.width;
	main.height = stage.height;
	stage.addChild(main);
	more.SliderUtils.createSliderBar(main);
	// 底部Banner，类型为1
	var banner = new more.MoreLogo(1);
	stage.addChild(banner);
	banner.x = 375; // banner.centerX = 0;
	banner.bottom = 0;

	// 广告正确使用示例：
	// var banner: more.MoreLogo;
	// var create = function () {
	// 	if (!banner) {
	// 		banner = new more.MoreLogo(1);
	// 		stage.addChild(banner);
	// 		banner.x = 375; // banner.centerX = 0;
	// 		banner.bottom = 0;
	// 	}
	// };
	// var remove = function () {
	//     if (banner) {
	//         stage.removeChild(banner);
	//         banner = null;
	//     }
	// };
	// var register = Dispatch.register;
	// 两个事件分别在广告对象的onLoad和onError响应
	// register(MsgStr.MS_ErrBanner, create);
	// register(MsgStr.MS_ShowBanner, remove);
});