more.MoreUtils.setConfig('https://static.xunguanggame.com/moreGame/sdks/model.json').then(function (config) {
    var scene = cc.director.getScene();
    var stage = new cc.Node;
    stage.setContentSize(750, 1334);
    stage.scale = .4;
    scene.addChild(stage);
    // 单个图标
    var logo = new more.MoreLogo();
    stage.addChild(logo);
    logo.x = 100;
    logo.y = 800;
    // 左边滑块-列表示例
    more.SliderUtils.createSliderBar(stage);
    // 底部Banner，类型为1，正确应由广告播放失败调用
    var banner = new more.MoreLogo(1);
    stage.addChild(banner);
    banner.x = 375;
    banner.y = banner.height / 2;
    banner.anchorY = 0;

    // 广告正确使用示例：
    // var banner;
    // var create = function () {
    //     if (!banner) {
    //         banner = new more.MoreLogo(1);
    //         stage.addChild(banner);
    //         banner.x = 375;
    //         banner.y = banner.height / 2;
    //         banner.anchorY = 0;
    //     }
    // };
    // var remove = function () {
    //     if (banner) {
    //         stage.removeChild(banner);
    //         banner = null;
    //     }
    // };
    // var register = Dispatch.register;
    // 两个事件分别在广告对象的onLoad和onError响应
    // register(MsgStr.MS_ErrBanner, create);
    // register(MsgStr.MS_ShowBanner, remove);
});