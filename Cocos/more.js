//// 小方法 ////
/**
 * 为对象添加某个属性的get set方法，可为空
 * @param obj 对象
 * @param attr 属性字符串
 * @param get get方法，可为空
 * @param set set方法，可为空
 */
var getset = function (obj, attr, get, set) {
    Object.defineProperty(obj, attr, {
        get,
        set,
        enumerable: true,
        configurable: true
    });
};
/**
 * 继承
 */
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
// Promise 0
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
// Promise 1
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function () { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function () { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
/**
 * 创建图片
 */
var createImg = function (source, sizeGrid, prob) {
    var node = new cc.Node, grids, frame;
    var sprite = node.addComponent(cc.Sprite);
    var setImg = more.SliderUtils.setImage;
    source && setImg(sprite, source);
    // 九宫格
    if (sizeGrid) {
        sprite.type = cc.Sprite.Type.SLICED;
        frame = sprite.spriteFrame;
        grids = sizeGrid.split(",").map(function (v) {
            return Number(v)
        });
        frame.insetTop = grids[0];
        frame.insetRight = grids[1];
        frame.insetBottom = grids[2];
        frame.insetLeft = grids[3];
    }
    prob && later(function () {
        for (let i in prob)
            node[i] = prob[i];
    });
    // 添加skin属性，抄袭Laya
    var skin;
    getset(node, "skin", function () {
        return skin;
    }, function (value) {
        if (skin != value) {
            // 保持宽高不变
            let size = node.getContentSize();
            skin = value;
            skin ? more.MoreUtils.getResByUrl(skin).then(function (texture) {
                sprite.spriteFrame = new cc.SpriteFrame(texture);
                node.setContentSize(size);
            }) : (sprite.spriteFrame = null);
        }
    });
    return node;
};
/**
 * 创建列表
 * @param item 子项的类
 * @param datas 数据源
 * @param width 宽
 * @param height 高
 * @param layout 布局属性
 */
var createList = function (item, datas, width, height, layoutProb) {
    var node = new cc.Node;
    var mask = new cc.Node;
    var list = new cc.Node;
    var scro = node.addComponent(cc.ScrollView);
    node.addChild(mask);
    mask.addChild(list);
    node.width = mask.width = list.width = width;
    node.height = mask.height = height;
    // 初始化
    scro.content = list;
    scro.horizontal = false;
    scro.elastic = true;
    scro.bounceDuration = .5;
    list.anchorY = 1;
    mask.addComponent(cc.Mask);
    // 测量
    var spX = layoutProb.spacingX || 0, spY = layoutProb.spacingY || 0;
    var init, col, row, size = datas.length, measure = function (cw, ch) {
        if (!init) {
            col = Math.floor(width / cw) || 1;
            row = Math.ceil(size / col) || 1;
            list.height = row * ch + (row - 1) * spY;
            init = true;
        }
    };
    for (let i in datas) {
        let child = new item;
        measure(child.width, child.height);
        let iCol = i % col;
        let iRow = (i / col | 0) + 1;
        child.x = iCol * (child.width + spX) - width / 2;
        child.y = -iRow * (child.height + spY);
        child.data = datas[i];
        list.addChild(child);
    }
    return node;
};

//// Timer ////
var more;
(function (more) {
    var count = 0;
    /**
     * 计时器，可启动、停止及统计已运行时间（单位毫秒），默认启动
     */
    var Timer = /** @class */ (function () {
        /**
         * 默认状态就是创建一个每一帧刷新一次的计时器
         * @param call 回调方法
         * @param thisObj 回调对象
         * @param delay 延迟，默认1，isTime为true时表示毫秒，否则表示帧数
         * @param isTime 是否时间回调，默认false（时间回调、帧回调）
         * @param isStop 是否不需要直接运行
         */
        function Timer(call, thisObj, delay, isTime, isStop) {
            if (delay === void 0 || delay < 1) { delay = 1; }
            this.$runTime = 0; // 已运行时间
            this.$runCount = 0; // 已运行次数
            this.$call = call;
            this.$thisObj = thisObj;
            isStop || this.start();
            isTime || (delay *= 50 / 3);    // 1s60帧
            this.$interval = more.setInterval(this.update, this, delay);
        }
        /**
         * 回调
         */
        Timer.prototype.update = function () {
            var self = this;
            if (self.$running) {
                self.$runCount++;
                self.$call.call(self.$thisObj);
            }
        };
        /**
         * 开始计时
         */
        Timer.prototype.start = function () {
            var self = this;
            if (!self.$running) {
                self.$lastTime = Date.now();
                self.$running = true;
            }
        };
        /**
         * 停止计时
         */
        Timer.prototype.stop = function () {
            var self = this;
            if (self.$running) {
                var nowT = Date.now();
                self.$runTime += nowT - self.$lastTime;
                self.$lastTime = nowT;
                self.$running = false;
            }
        };
        Object.defineProperty(Timer.prototype, "running", {
            /**
             * 获取是否运行中
             */
            get: function () {
                return this.$running;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Timer.prototype, "runTime", {
            /**
             * 获取运行的时间
             */
            get: function () {
                var self = this;
                return self.$runTime + (self.running ?
                    Date.now() - self.$lastTime : 0);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Timer.prototype, "runCount", {
            /**
             * 获取运行的次数（执行回调的次数）
             */
            get: function () {
                return this.$runCount;
            },
            /**
             * 设置运行的次数(value>=0)，慎用
             */
            set: function (value) {
                this.$runCount = value > 0 ? +value : 0;
            },
            enumerable: true,
            configurable: true
        });
        /**
         * 重置时间，归0
         */
        Timer.prototype.reset = function () {
            var self = this;
            self.$runTime = self.$runCount = 0;
            self.$lastTime = Date.now();
        };
        /**
         * 清除定时器，一经清除，将不可再用
         */
        Timer.prototype.clear = function () {
            var self = this;
            clearInterval(self.$interval);
            self.$interval = self.$call = self.$thisObj = null;
        };
        return Timer;
    } ());
    more.Timer = Timer;
    /**
     * 模仿setTimeout
     * @param call
     * @param thisObj
     * @param delay
     */
    more.setTimeout = function (call, thisObj, delay) {
        var param = [];
        for (var _i = 3; _i < arguments.length; _i++) {
            param[_i - 3] = arguments[_i];
        }
        isNaN(delay) && (delay = 0);
        return window.setTimeout(function () {
            call.apply(thisObj, param);
        }, delay);
    };
    /**
     * 清除延迟回调
     * @param key 标志
     */
    more.clearTimeout = window.clearTimeout;
    /**
     * 设置间隔回调
     * @param call 回调函数
     * @param thisObj 回调所属对象
     * @param delay 回调间隔，默认0
     */
    more.setInterval = function (call, thisObj, delay) {
        var param = [];
        for (var _i = 3; _i < arguments.length; _i++) {
            param[_i - 3] = arguments[_i];
        }
        isNaN(delay) && (delay = 0);
        return window.setInterval(function () {
            call.apply(thisObj, param);
        }, delay);
    };
    /**
     * 清除间隔回调
     * @param key
     */
    more.clearInterval = window.clearInterval;
})(more || (more = {}));

//// Tween ////
var more;
(function (more) {
    var cache = '$LTween', pool = []; // 缓存标志
    /**
     * 缓间动画
     */
    var Tween = /** @class */ (function () {
        function Tween() {
        }
        /**
         * 初始化基础属性
         */
        Tween.prototype.$init = function (target, loop, once, frameCall, frameObj) {
            var self = this;
            target[cache] = self; // 清理标志
            self.$target = target;
            self.$loop = loop;
            self.$once = once;
            self.$frameCall = frameCall;
            self.$frameObj = frameObj;
            // 数据初始化
            self.$curTime = 0;
            self.$needCopy = true;
            self.$steps = [];
            self.$cSteps = [];
            // 启动定时器，默认不运行
            self.$timer = new more.Timer(self.$update, self, 1, false, true);
        };
        /**
         * 定时器回调
         */
        Tween.prototype.$update = function () {
            var self = this;
            var steps = self.$steps;
            // 检测复制执行顺序
            if (self.$needCopy) {
                self.$needCopy = false;
                more.MoreUtils.insert(self.$cSteps, steps);
            }
            // 执行
            var runTime = self.$timer.runTime, remove = 0;
            for (var i = 0, len = steps.length; i < len; i++) {
                var step = steps[i];
                if (step.startTime > runTime)
                    break;
                self.$runStep(step);
                // 运行结束
                if (step.endTime <= runTime)
                    remove++;
            }
            // 移除执行完毕
            remove > 0 && steps.splice(0, remove);
            // 执行帧回调
            var call = self.$frameCall;
            call && call.call(self.$frameObj);
            // 执行结束：注意需要用self.$steps来判断，因为$runStep可能将Tween给变质了，导致steps不等于self.$step
            steps = self.$steps;
            if (steps && steps.length == 0) {
                if (self.$loop) {
                    self.$timer.reset();
                    self.$steps = self.$cSteps.concat();
                }
                else {
                    self.$once ? self.clear() : self.$pause();
                }
            }
        };
        /**
         * 静止，时间归0
         */
        Tween.prototype.$pause = function () {
            var self = this;
            var timer = self.$timer;
            self.$needCopy = true;
            self.$curTime = 0;
            self.$steps = [];
            timer.stop();
            timer.reset();
        };
        /**
         * 添加步骤
         * @param type 类型
         * @param duration 持续时间
         */
        Tween.prototype.$addStep = function (type, duration, param) {
            var self = this;
            var startTime = self.$curTime;
            var endTime = self.$curTime = startTime + duration;
            self.$steps.push({ type: type, startTime: startTime, endTime: endTime, param: param });
            self.$timer.start(); // 自动启动
        };
        /**
         * 获取属性增量，若起始属性没有结束属性的值，则忽略该属性
         * @param start 起始属性
         * @param end 结束属性
         */
        Tween.prototype.$getIncrement = function (start, end) {
            var copy = {};
            var keys = Object.keys(end);
            var hasv = more.MoreUtils.hasValue;
            for (var i in keys) {
                var key = keys[i];
                var value = start[key];
                if (hasv(value))
                    copy[key] = end[key] - value;
            }
            return copy;
        };
        /**
         * 运行步骤
         */
        Tween.prototype.$runStep = function (step) {
            var self = this;
            var type = step.type;
            switch (type) {
                case 0:
                    self.$to(step);
                    break;
                case 1:
                    self.$set(step.param);
                    break;
                case 2:
                    break;
                case 3:
                    self.$call(step.param);
                    break;
            }
        };
        /**
         * 属性渐变
         */
        Tween.prototype.$to = function (step) {
            var self = this;
            // 实际经过时间比例
            var start = step.startTime;
            var ratio = Math.min((self.$timer.runTime - start) / (step.endTime - start), 1);
            // 修改比例
            var param = step.param;
            var ease = param[0];
            ease && (ratio = ease(ratio));
            // 初始化属性
            var target = self.$target, endp = param[1], dstp = param[2] || (param[2] = self.$getIncrement(target, endp));
            // 修改属性
            for (var i in dstp) {
                target[i] = endp[i] - dstp[i] * (1 - ratio);
            }
        };
        /**
         * 复制属性
         */
        Tween.prototype.$set = function (props) {
            var self = this;
            var target = self.$target;
            for (var i in props)
                target[i] = props[i];
        };
        /**
         * 执行回调
         */
        Tween.prototype.$call = function (param) {
            param[0].apply(param[1], param[2]);
        };
        //// 供使用方法 ////
        /**
         * 执行到对应的属性
         * @param props 对象属性集合，一般都是属性值都是数字
         * @param duration 持续时间，非负数，建议时间不低于一帧
         * @param ease 缓动算法
         */
        Tween.prototype.to = function (props, duration, ease) {
            var self = this;
            if (isNaN(duration) || duration <= 0) {
                self.set(props);
            }
            else {
                self.$addStep(0, duration, [ease, props]);
            }
            return self;
        };
        /**
         * 直接修改对象属性
         * @param props 对象属性集合
         */
        Tween.prototype.set = function (props) {
            var self = this;
            self.$addStep(1, 0, props);
            return self;
        };
        /**
         * 等待
         * @param delay 毫秒数，大于0才有效
         */
        Tween.prototype.wait = function (delay) {
            var self = this;
            delay > 0 && self.$addStep(2, delay);
            return self;
        };
        /**
         * 执行回调
         * 注：尽量避免在回调里对tween进行有持续性的操作to/wait等，会出现异常现象
         */
        Tween.prototype.call = function (call, thisObj) {
            var param = [];
            for (var _i = 2; _i < arguments.length; _i++) {
                param[_i - 2] = arguments[_i];
            }
            var self = this;
            call && self.$addStep(3, 0, [call, thisObj, param]);
            return self;
        };
        //// 特殊方法，不进行拼接，一般用于循环动画或者长时间的动画调用 ////
        /**
         * 停止运行，不销毁
         */
        Tween.prototype.stop = function () {
            var timer = this.$timer;
            timer && timer.stop();
        };
        /**
         * 恢复运行
         */
        Tween.prototype.resume = function () {
            var timer = this.$timer;
            timer && timer.start();
        };
        /**
         * 清理
         */
        Tween.prototype.clear = function () {
            var self = this;
            if (self.$timer) {
                self.$target[cache] = null; // 清理标志
                self.$timer.clear();
                self.$timer = self.$steps = self.$cSteps = self.$target = self.$frameCall = self.$frameObj = null;
                pool.push(self);
            }
        };
        /**
         * 获取一个缓间动画对象
         * 销毁条件：once为true且非循环调用时运行结束会自动销毁，否之请自行调用clear（两个任选一个都行）来清理
         * @param target 对象
         * @param props 动画属性：
         * 1.loop：是否循环执行，默认false；
         * 2.once：是否自动销毁，默认true，loop为true时该属性无效；
         * 3.frameCall：帧回调，属性变化后调用
         * 4.frameObj：帧回调所属对象
         */
        Tween.get = function (target, props) {
            var tween = pool.pop() || new Tween;
            !props && (props = {});
            props.once == void 0 && (props.once = true);
            Tween.clear(target); // 清除旧动画
            tween.$init(target, props.loop, props.once, props.frameCall, props.frameObj);
            return tween;
        };
        /**
         * 移除对象的动画
         * @param target 对象
         */
        Tween.clear = function (target) {
            var tween = target && target[cache];
            tween instanceof Tween && tween.clear();
        };
        return Tween;
    } ());
    more.Tween = Tween;
    var Ease = /** @class */ (function () {
        function Ease() {
        }
        /**
         * 时间的幂次方变化
         * @param pow 幂次方
         */
        Ease.getPowIn = function (pow) {
            return function (t) {
                return Math.pow(t, pow);
            };
        };
        /**
         * 反幂次方变化
         * @param pow 幂次方
         */
        Ease.getPowOut = function (pow) {
            return function (t) {
                return 1 - Math.pow(1 - t, pow);
            };
        };
        ;
        /**
         * 反弹簧变化
         * @param amplitude 振幅范围
         * @param period 恢复时间
         */
        Ease.getElasticOut = function (amplitude, period) {
            var pi2 = Math.PI * 2;
            return function (t) {
                if (t == 0 || t == 1)
                    return t;
                var s = period / pi2 * Math.asin(1 / amplitude);
                return (amplitude * Math.pow(2, -10 * t) * Math.sin((t - s) * pi2 / period) + 1);
            };
        };
        ;
        /**
         * 幂次方正反变化
         */
        Ease.getPowInOut = function (pow) {
            return function (t) {
                if ((t *= 2) < 1)
                    return 0.5 * Math.pow(t, pow);
                return 1 - 0.5 * Math.abs(Math.pow(2 - t, pow));
            };
        };
        /**
         * 2次平方变化
         */
        Ease.quadIn = Ease.getPowIn(2);
        /**
         * 反2次平方变化
         */
        Ease.quadOut = Ease.getPowOut(2);
        /**
         * 反弹簧
         */
        Ease.elasticOut = Ease.getElasticOut(1, 0.3);
        /**
         * 2次方正反变化
         */
        Ease.quadInOut = Ease.getPowInOut(2);
        return Ease;
    } ());
    more.Ease = Ease;
})(more || (more = {}));

//// Http ////
// var more;
// (function (more) {
//     var getM = 'GET';
//     var postM = 'POST';
//     var load;
//     /**
//      * Http请求类
//      * 注：如果请求失败，会返回字符串，请用catch抓取
//      */
//     var Http = /** @class */ (function () {
//         function Http(url, method, param, type, noLoading) {
//             var request = this.$request = cc.loader.getXMLHttpRequest();
//             var isBuffer = type == 0;
//             this.isJson = !(isBuffer || type == 1);
//             this.hasLoding = !noLoading;
//             request.responseType = isBuffer ? 'arraybuffer' : 'text';
//             request.open(method, url, true);
//             request.send(Http.getUrlParam(param));
//         }
//         /**
//          * 添加返回请求
//          */
//         Http.prototype.addCall = function (call, error, thisObj) {
//             var self = this;
//             var request = self.$request;
//             self.$comCall = call;
//             self.$errorCall = error;
//             self.$thisObj = thisObj;
//             self.$loading = more.setTimeout(self.showLoading, self, 400);
//             // 请求监听
//             request.onreadystatechange = function () {
//                 if (request.readyState == 4) {
//                     if (request.status >= 200 && request.status < 400) {
//                         self.onGetComplete();
//                     } else {
//                         self.onGetIOError();
//                     }
//                 }
//             };
//         };
//         /**
//          * 请求成功
//          */
//         Http.prototype.onGetComplete = function () {
//             var self = this;
//             var response = self.$request.responseText;
//             try {
//                 var data = self.isJson ? JSON.parse(response) : response;
//                 self.excuteCall(data);
//             }
//             catch (e) {
//                 self.excuteCall('非JSON格式：' + response, true);
//             }
//         };
//         /**
//          * 请求失败
//          */
//         Http.prototype.onGetIOError = function () {
//             this.excuteCall('连接服务器失败', true);
//         };
//         /**
//          * 执行回调
//          */
//         Http.prototype.excuteCall = function (data, isError) {
//             var self = this;
//             var call = isError ? self.$errorCall : self.$comCall;
//             call && call.call(self.$thisObj, data);
//             self.clear();
//             // 关闭
//             if (self.$loading) {
//                 more.clearTimeout(self.$loading);
//                 self.$loading = null;
//             }
//             else if (self.$timeout) {
//                 more.clearTimeout(self.$timeout);
//                 self.$timeout = null;
//                 self.hasLoding && load && load.hideLoading();
//             }
//         };
//         /**
//          * 显示loading
//          */
//         Http.prototype.showLoading = function () {
//             var self = this;
//             self.$loading = null;
//             self.hasLoding && load && load.showLoading();
//             // 超时检测
//             self.$timeout = more.setTimeout(self.onGetIOError, self, 10000);
//         };
//         /**
//          * 清除请求
//          */
//         Http.prototype.clear = function () {
//             var self = this;
//             self.$request.onreadystatechange = null;
//             self.$request = self.$comCall = self.$errorCall = self.$thisObj = null;
//         };
//         //// 静态方法 ////
//         /**
//          * 发起请求
//          */
//         Http.$request = function (data, method) {
//             var http = new Http(data.url, method, data.param, data.type, data.noLoading);
//             return new Promise(function (resolve, reject) {
//                 http.addCall(resolve, reject);
//             });
//         };
//         /**
//          * 设置请求中loading的工具
//          * @param newLoad 新的loading工具
//          */
//         Http.setLoading = function (newLoad) {
//             load = newLoad;
//         };
//         /**
//          * 将对象转为伴随这url的参数
//          * @param param 参数对象
//          * @param sortFunc 对象key值的排序
//          */
//         Http.getUrlParam = function (param, sortFunc) {
//             if (param) {
//                 var str = JSON.stringify;
//                 var attr = [];
//                 for (var i in param) {
//                     var data = param[i];
//                     if (typeof data === 'object') {
//                         data = str(data);
//                     }
//                     attr.push(i + '=' + data);
//                 }
//                 sortFunc && attr.sort(sortFunc);
//                 return attr.join('&');
//             }
//             return '';
//         };
//         /**
//          * 获取网页url上的参数
//          */
//         Http.getQueryString = function (name) {
//             var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
//             var ret = window.location.search.substr(1).match(reg);
//             return ret ? decodeURIComponent(ret[2]) : '';
//         };
//         /**
//          * 发起post请求
//          * @param data 参数
//          */
//         Http.post = function (data) {
//             return Http.$request(data, postM);
//         };
//         /**
//          * 发起get请求
//          * @param data 参数
//          */
//         Http.get = function (data) {
//             return Http.$request(data, getM);
//         };
//         return Http;
//     } ());
//     more.Http = Http;
// })(more || (more = {}));

//// RepeatUtils ////
var more;
(function (more) {
    /**
     * 重复执行某一操作工具类，一般用于定时发起网络请求用。一次性工具，用完不复用
     * 定时：上一次操作完成时和下一次操作开始时的时间间隔，而非两个操作开始时的时间间隔；
     * 功能：定时发起重复操作回调，当回调结果为true时结束回调，并通知成功；
     * 当在规定上限内还未收到true的话也会结束回调，并通知失败
     */
    var RepeatUtils = /** @class */ (function () {
        /**
         * @param repeatTime 单位毫秒，即每隔多久发起一次操作
         * @param repeatCount 操作次数上限，默认一次
         */
        function RepeatUtils(repeatTime, repeatCount) {
            if (repeatCount === void 0) { repeatCount = 1; }
            this.repeatTime = repeatTime;
            this.$maxCount = this.repeatCount = isNaN(repeatCount) ? 0 : repeatCount;
        }
        /**
         * 定时器回调
         */
        RepeatUtils.prototype.onTimer = function () {
            var self = this;
            var curT = Date.now();
            if (curT - self.$startTime >= self.repeatTime && !self.isRunning) {
                var endCall_1 = function () {
                    self.isRunning = false;
                    if (!(--self.repeatCount > 0)) {
                        self.onFinish(false);
                    }
                };
                self.$startTime = curT;
                self.isRunning = true;
                // 创建promise
                var promise = self.$call.call(self.$thisObj, self.$maxCount - self.repeatCount);
                if (promise instanceof Promise) {
                    promise.then(function (bool) {
                        bool ? self.onFinish(true) : endCall_1();
                    }).catch(endCall_1);
                }
                else {
                    endCall_1();
                }
            }
        };
        /**
         * 回调结束
         * @param bool 操作结果
         */
        RepeatUtils.prototype.onFinish = function (bool) {
            var self = this;
            var timer = self.$timer;
            self.isRunning = false;
            if (timer) {
                var fCall = self.$fCall;
                fCall && fCall.call(self.$fThisObj, bool);
                self.$timer.clear();
                self.$call = self.$thisObj = self.$fCall = self.$fThisObj = self.$timer = null;
            }
        };
        /**
         * 设置结束回调，不设置也不会怎么样
         * @param call 参数表示是否正常结束
         */
        RepeatUtils.prototype.setFinishCall = function (call, thisObj) {
            var self = this;
            self.$fCall = call;
            self.$fThisObj = thisObj;
        };
        /**
         * 设置重复回调，并自动开始。建议不要重复设置
         * @param call 根据运行次数返回Promise对象
         */
        RepeatUtils.prototype.setRepeatCall = function (call, thisObj) {
            var self = this;
            if (!self.$timer && self.$call !== call) {
                self.$call = call;
                self.$thisObj = thisObj;
                if (call && self.repeatCount > 0) {
                    self.$startTime = 0;
                    self.onTimer(); // 先执行一次
                    self.$timer = new more.Timer(self.onTimer, self, self.repeatTime, true);
                }
            }
        };
        return RepeatUtils;
    } ());
    more.RepeatUtils = RepeatUtils;
})(more || (more = {}));

//// MoreUtils ////
var more;
(function (more) {
    var CEvent = cc.Node.EventType;
    /**
     * 更多游戏工具类，微信交接工具
     */
    var MoreUtils = /** @class */ (function () {
        function MoreUtils() {
        }
        /**
         * 循环左右摇晃动画，注意控件必须锚点居中或者有水平垂直约束，否则会很怪异
         * @param time 第一段动画时间
         */
        MoreUtils.swing = function (target, time) {
            if (time === void 0) { time = 100; }
            more.Tween.get(target, { loop: true }).to({
                rotation: 30
            }, time).to({
                rotation: -30
            }, time * 1.6).to({
                rotation: 15
            }, time * 1.2).to({
                rotation: -15
            }, time * .8).to({
                rotation: 0
            }, time * .6).wait(time * 8);
        };
        //// 监听相关 ////
        /**
         * 移除所有监听
         */
        MoreUtils.removeEventListener = function (target) {
            target._bubblingListeners = target._capturingListeners = null;
        };
        /**
         * 移除root往下所有的点击事件
         */
        MoreUtils.removeEventListeners = function (root) {
            var self = MoreUtils;
            var children = root._children;
            for (let i in children)
                self.removeEventListeners(children[i]);
            self.removeEventListener(root);
        };
        /**
         * 添加点击按下监听
         */
        MoreUtils.addTouchStartListener = function (target, call, thisObj) {
            target.on(CEvent.TOUCH_START, call, thisObj);
        };
        /**
         * 添加监听取消监听
         */
        MoreUtils.addTouchCancelListener = function (target, finish, thisObj) {
            target.on(CEvent.TOUCH_CANCEL, finish, thisObj);
        };
        /**
         * 添加TouchEnd监听
         */
        MoreUtils.addTouchEndListener = function (target, call, thisObj, useCapture) {
            target.on(CEvent.TOUCH_END, call, thisObj);
        };
        /**
         * 添加缩放监听，记得用removeEventListener来移除这个监听
         */
        MoreUtils.addScaleListener = function (target, scale) {
            if (scale === void 0) { scale = 0.95; }
            var self = MoreUtils;
            target.$evtScale = scale;
            target.$bgScaleX = target.scaleX;
            target.$bgScaleY = target.scaleY;
            self.addTouchStartListener(target, self.onScaleBegin, self);
            self.addTouchCancelListener(target, self.onScaleEnd, self);
            self.addTouchEndListener(target, self.onScaleEnd, self);
        };
        /**
         * 缩放开始
         */
        MoreUtils.onScaleBegin = function (event) {
            var target = event.currentTarget;
            var tween = more.Tween;
            var scale = target.$evtScale;
            var scaleX = target.scaleX = target.$bgScaleX;
            var scaleY = target.scaleY = target.$bgScaleY;
            scaleX *= scale;
            scaleY *= scale;
            target.$hashBegin = true;
            tween.get(target).to({ scaleX: scaleX, scaleY: scaleY }, 100);
        };
        /**
         * 缩放结束
         */
        MoreUtils.onScaleEnd = function (event) {
            var target = event.currentTarget;
            if (target.$hashBegin) {
                var time = 100;
                var scaleX = target.$bgScaleX;
                var scaleY = target.$bgScaleY;
                var bScaleX = scaleX * 1.1;
                var bScaleY = scaleY * 1.1;
                more.Tween.get(target).to({ scaleX: bScaleX, scaleY: bScaleY }, time).
                    to({ scaleX: scaleX, scaleY: scaleY }, time);
                target.$hashBegin = void 0;
            }
        };
        /**
         * 在TouchEnd的基础上进行缩放
         */
        MoreUtils.addTouchEndScaleListener = function (target, call, thisObj, scale) {
            var self = MoreUtils;
            self.addScaleListener(target, scale);
            self.addTouchEndListener(target, call, thisObj);
        };
        //// 监听结束 ////
        /**
         * 跳转到小程序
         * @param appId 小程序的appId
         * @param path 跳转路径
         * @param extraData 扩展消息
         * @param preImg 低版本弹出图片
         */
        MoreUtils.toMiniProgram = function (appId, path, preImg) {
            var toMini = wx.navigateToMiniProgram;
            if (toMini) {
                return new Promise(function (resolve) {
                    toMini({
                        appId: appId,
                        path: path,
                        envVersion: 'trial',
                        // extraData: extraData,
                        success: function (res) {
                            resolve(true);
                        },
                        fail: function (res) {
                            MoreUtils.previewImage(preImg).then(resolve);
                        }
                    });
                });
            }
            else
                return MoreUtils.previewImage(preImg);
        };
        /**
         * 显示图片
         */
        MoreUtils.previewImage = function (url) {
            return new Promise(function (resolve) {
                url ? wx.previewImage({
                    urls: [url],
                    success: function () {
                        resolve(true);
                    },
                    fail: function () {
                        resolve(false);
                    }
                }) : resolve(false);
            });
        };
        /**
         * 检测文件格式
         * @param url 文件地址
         * @param format 格式，建议首字符为'.'，如'.png'、'.json'等
         */
        MoreUtils.checkFile = function (url, format) {
            return url.indexOf(format) == url.length - format.length;
        };
        /**
         * 将source的内容全部插入target
         * @param target 目标
         * @param source 来源
         * @param inHead 是否插在头部
         */
        MoreUtils.insert = function (target, source, inHead) {
            target[inHead ? 'unshift' : 'push'].apply(target, source);
        };
        /**
         * 是否有值
         */
        MoreUtils.hasValue = function (obj) {
            // 右侧兼容空字符串和0
            return !!obj || (obj != null && obj != void 0);
        };
        /**
         * 根据网络地址加载资源
         * @param url
         */
        MoreUtils.getResByUrl = function (url) {
            return new Promise(function (resolve) {
                cc.loader.load(url, function (err, data) {
                    resolve(data);
                });
            });
        };
        /**
         * 设置SDK配置，需要优先调用
         * @param url sdk配置路径
         */
        MoreUtils.setConfig = function (url) {
            var self = MoreUtils;
            var repeate = new more.RepeatUtils(1000, 5);
            var call = function () {
                return new Promise(function (resolve) {
                    self.getResByUrl(url).then(function (data) {
                        // 刷新配置操作
                        var uTime = data.updateTime;
                        uTime > 0 && more.setTimeout(self.setConfig, self, uTime);
                        // 返回
                        self.config = data;
                        resolve(true);
                    }).catch(function () {
                        resolve(false);
                    });
                });
            };
            repeate.setRepeatCall(call);
            return new Promise(function (resolve, reject) {
                repeate.setFinishCall(function (bool) {
                    bool ? resolve(self.config) : reject();
                });
            });
        };
        return MoreUtils;
    } ());
    more.MoreUtils = MoreUtils;
})(more || (more = {}));

//// ClipImage ////
var more;
(function (more) {
    /**
     * 切片图，帧动画
     */
    var ClipImage = /** @class */ (function (_super) {
        __extends(ClipImage, _super);
        function ClipImage() {
            var _this = _super.call(this) || this;
            _this.$image = _this.addComponent(cc.Sprite);
            _this.on(cc.Director.EVENT_BEFORE_SCENE_LOADING, _this.destroy, _this);
            return _this;
        }
        /**
         * 离开舞台调用，重写前需注意不要删除原有功能
         */
        ClipImage.prototype.destroy = function () {
            var self = this;
            self.$size = null;
            self.stop();
            more.MoreUtils.removeEventListener(self);
            self.onDestroy();
            _super.prototype.destroy.call(self);
        };
        /**
         * 修改纹理调用
         */
        ClipImage.prototype.onChange = function () {
        };
        /**
         * 离开舞台调用
         */
        ClipImage.prototype.onDestroy = function () {
        };
        /**
         * 设置切片
         * @param data 帧数据
         * @param sheet 合图
         */
        ClipImage.prototype.setClip = function (data, sheet) {
            var self = this;
            // 创建子图
            var res = data.res;
            var frames = self.$frames = {};
            for (var i in res) {
                var item = res[i];
                frames[i] = new cc.SpriteFrame(sheet, cc.rect(item.x, item.y, item.w, item.h));
            }
            self.$mcData = data.mc;
        };
        /**
         * 播放
         * @param aniName 动画名称，为空则选择首个动画
         */
        ClipImage.prototype.play = function (aniName) {
            var self = this;
            var mcData = self.$mcData, frames = self.$frames;
            if (mcData && frames) {
                aniName || (aniName = Object.keys(mcData)[0]);
                // 动画开始
                var frame = aniName && mcData[aniName];
                if (frame) {
                    var index_1 = 0;
                    var speed = 1000 / (frame.frameRate || 5);
                    var frames_1 = frame.frames;
                    var frmLen_1 = frames_1.length;
                    var call = function () {
                        self.source = frames[frames_1[index_1++].res];
                        index_1 %= frmLen_1;
                        // 奇葩的报错
                        self.parent || self.clear();
                    };
                    call(); // 先执行一次
                    self.$interval = more.setInterval(call, null, speed);
                }
            }
        };
        /**
         * 停止播放
         */
        ClipImage.prototype.stop = function () {
            var self = this;
            if (self.$interval) {
                more.clearInterval(self.$interval);
                self.$interval = null;
            }
        };
        /**
         * 设置尺寸，设置纹理后也保持
         */
        ClipImage.prototype.setSize = function (width, height) {
            var self = this;
            self.$size = width != void 0 && [width, height];
        };
        /**
         * 设置spriteFrame
         */
        getset(ClipImage.prototype, "source", void 0, function (frame) {
            var self = this;
            var image = self.$image;
            if (image.spriteFrame != frame) {
                var size = self.$size;
                image.spriteFrame = frame;
                size && (self.width = size[0], self.height = size[1]);
                self.onChange();
            }
        });
        /**
         * 设置皮肤，自动加载url地址
         */
        getset(ClipImage.prototype, "skin", void 0, function (skin) {
            var self = this;
            skin ? more.MoreUtils.getResByUrl(skin).then(function (texture) {
                self.source = texture && new cc.SpriteFrame(texture);
            }) : (self.source = null);
        });
        return ClipImage;
    } (cc.Node));
    more.ClipImage = ClipImage;
})(more || (more = {}));

//// MoreLogo ////
var more;
(function (more) {
    /**
     * 更多游戏——单个logo
     * 注：该logo的锚点位于中心点，因此加入场景时注意坐标
     */
    var MoreLogo = /** @class */ (function (_super) {
        __extends(MoreLogo, _super);
        function MoreLogo(type) {
            var _this = _super.call(this) || this;
            _this.initView(more.MoreUtils.config, type);
            return _this;
        }
        /**
         * 初始化界面
         */
        MoreLogo.prototype.initView = function (config, type) {
            var self = this;
            // 显示Logo
            var isBanner = type == 1;
            var games = self.$games = config[isBanner ? 'banner' : 'games'];
            var length = games && games.length;
            if (length > 0) {
                var clzz = more.MoreUtils;
                // 先刷一次
                self.$index = -1;
                self.updateIndex();
                // 轮询
                if (length > 1) {
                    var pollTime = config.pollTime;
                    if (!(pollTime > 0))
                        pollTime = 30000;
                    self.$updateInt = more.setInterval(self.updateIndex, self, pollTime);
                }
                // 其他
                isBanner || clzz.swing(self);
                clzz.addTouchEndListener(self, self.onClick, self);
            }
            else
                self.skin = '';
        };
        /**
         * 更新logo下标
         */
        MoreLogo.prototype.updateIndex = function () {
            var self = this;
            var games = self.$games;
            var index = self.$index = (self.$index + 1) % games.length;
            var game = games[index], logo = game.logo;
            // 初始化
            self.$initAc = false;
            self.clearMovie();
            // 尺寸
            var size = game.size;
            if (size) {
                self.setSize(size.w, size.h);
                self.$initAc = true;
            }
            else {
                self.setSize();
            }
            // 动画
            if (more.MoreUtils.checkFile(logo, '.json'))
                self.initMovie(logo.substr(0, logo.length - 5));
            else
                self.skin = logo;
        };
        /**
         * 初始化动画
         */
        MoreLogo.prototype.initMovie = function (name) {
            return __awaiter(this, void 0, void 0, function () {
                var self, getRes, pngUrl, frame, sheet;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            self = this;
                            getRes = more.MoreUtils.getResByUrl;
                            pngUrl = name + '.png';
                            return [4 /*yield*/, getRes(name + '.json')];
                        case 1:
                            frame = _a.sent();
                            return [4 /*yield*/, getRes(pngUrl)];
                        case 2:
                            sheet = _a.sent();
                            self.setClip(frame, sheet);
                            self.play();
                            return [2 /*return*/];
                    }
                });
            });
        };
        /**
         * 清除动画定时器
         */
        MoreLogo.prototype.clearMovie = function () {
            var self = this;
            self.stop();
            self.skin = '';
        };
        /**
         * 点击
         */
        MoreLogo.prototype.onClick = function () {
            var config = this.$games[this.$index];
            config && more.MoreUtils.toMiniProgram(config.appId, config.path, config.preImg);
        };
        /**
         * 清除事件、动画
         */
        MoreLogo.prototype.clear = function () {
            var self = this;
            self.skin = '';
            more.Tween.clear(self);
            self.off(cc.Node.EventType.TOUCH_END);
            more.clearInterval(self.$updateInt);
        };
        /**
         * 重写
         */
        MoreLogo.prototype.onDestroy = function () {
            this.clear();
        };
        return MoreLogo;
    } (more.ClipImage));
    more.MoreLogo = MoreLogo;
})(more || (more = {}));

//// SliderItem0 ////
var more;
(function (more) {
    /**
     * 滑块列表子项0
     */
    var SliderItem0 = /** @class */ (function (_super) {
        __extends(SliderItem0, _super);
        function SliderItem0() {
            var _this = _super.call(this) || this;
            var width = _this.width = 560;
            var height = _this.height = 134;
            var image = createImg('pic_grade_sidebar_png', '60,75,60,75', { width: width });
            image.x = width / 2;
            image.y = height / 2;
            _this.addChild(image);
            // logo
            var image = _this.m_imgLogo = createImg();
            image.x = 75;
            image.y = 67;
            image.width = image.height = 110;
            _this.addChild(image);
            // 遮罩
            var image = createImg('game_mask_png');
            image.x = 75;
            image.y = 67;
            image.width = image.height = 110;
            _this.addChild(image);
            // 名称
            var node = new cc.Node;
            var label = _this.m_lblName = node.addComponent(cc.Label);
            node.anchorX = 0;
            node.x = 162;
            node.y = 90;
            label.fontSize = 28;
            node.color = new cc.Color(38, 16, 9);
            _this.addChild(node);
            // 钻石
            var image = createImg('pic_count_sidebar_png');
            image.x = 236;
            image.y = 44;
            _this.addChild(image);
            // 数量
            var node = new cc.Node;
            var label = _this.m_lblNum = node.addComponent(cc.Label);
            node.x = width / 2 - 30;
            node.y = 40;
            label.fontSize = 28;
            node.color = new cc.Color(167, 79, 7);
            _this.addChild(node);
            // 按钮
            var image = _this.m_imgRec = createImg('btn_get_sidebar_png');
            image.x = width / 2 + 173;
            image.y = height / 2;
            _this.addChild(image);
            more.MoreUtils.addTouchEndScaleListener(image, _this.onClick, _this);
            return _this;
        }
        /**
         * 界面刷新，可手动调用强制刷新
         */
        SliderItem0.prototype.update = function () {
            var self = this;
            var data = self.$data;
            if (data) {
                self.m_imgLogo.skin = data.logo;
                self.m_lblName.string = data.name;
                self.m_lblNum.string = (data.number || 500) + '';
            }
        };
        /**
         * 自定义设置数据
         */
        getset(SliderItem0.prototype, "data", void 0, function (data) {
            var self = this;
            self.$data = data;
            self.update();
        });
        /**
         * 点击
         */
        SliderItem0.prototype.onClick = function () {
            var config = this.$data;
            more.MoreUtils.toMiniProgram(config.appId, config.path, config.preImg);
        };
        return SliderItem0;
    } (cc.Node));
    more.SliderItem0 = SliderItem0;
})(more || (more = {}));

//// SliderItem1 ////
var more;
(function (more) {
    /**
     * 滑块列表子项1
     */
    var SliderItem1 = /** @class */ (function (_super) {
        __extends(SliderItem1, _super);
        function SliderItem1() {
            var _this = _super.call(this) || this;
            var set = more.SliderUtils.setImage;
            _this.width = _this.height = 120;
            // logo
            var image = _this.m_imgLogo = createImg();
            image.anchorX = 0;
            image.anchorY = 0;
            image.width = image.height = 110;
            _this.addChild(image);
            // 遮罩
            var image = createImg('game_mask_png');
            image.anchorX = 0;
            image.anchorY = 0;
            image.width = image.height = 110;
            _this.addChild(image);
            // 红点
            var image = _this.m_imgDot = createImg('pic_new_sidebar_png');
            image.anchorX = 0;
            image.anchorY = 0;
            image.x = 96;
            image.y = 94;
            _this.addChild(image);
            // 监听
            more.MoreUtils.addTouchEndListener(_this, _this.onClick, _this);
            return _this;
        }
        /**
         * 自定义设置数据
         */
        getset(SliderItem1.prototype, "data", void 0, function (data) {
            var self = this;
            self.$data = data;
            self.update();
        });
        /**
         * 点击
         */
        SliderItem1.prototype.onClick = function () {
            var config = this.$data;
            more.MoreUtils.toMiniProgram(config.appId, config.path, config.preImg);
        };
        /**
         * 刷新界面
         */
        SliderItem1.prototype.update = function () {
            var self = this;
            var data = self.$data;
            if (data) {
                self.m_imgLogo.skin = data.logo;
                self.m_imgDot.visible = data.reddot;
            }
        };
        return SliderItem1;
    } (cc.Node));
    more.SliderItem1 = SliderItem1;
})(more || (more = {}));

//// SliderList ////
var more;
(function (more) {
    /**
     * 滑块列表
     */
    var SliderList = /** @class */ (function (_super) {
        __extends(SliderList, _super);
        function SliderList() {
            var _this = _super.call(this) || this;
            var config = more.MoreUtils.config;
            var width = _this.width = 750;
            var height = _this.height = 1334;// cc.winSize.height;
            // 透明背景
            var rect = _this.m_rtBg = new cc.Node;
            var graphics = rect.addComponent(cc.Graphics);
            graphics.fillColor = new cc.Color(0, 0, 0, 102);
            graphics.fillRect(0, 0, width, height);
            _this.addChild(rect);
            // 内容
            var group = new cc.Node;
            var gWidth = group.width = 666;
            var gHeight = group.height = 1178;
            group.x = (width - gWidth) / 2;
            group.y = (height - gHeight) / 2;
            _this.addChild(group);
            // 背景
            var image = createImg('pic_popbg_sidebar_png', '120,0,90,0', { height: 1020 });
            image.x = gWidth / 2;
            image.y = gHeight - 1020 / 2;
            group.addChild(image);
            // 关闭按钮
            var image = _this.m_imgClose = createImg('btn_close_sidebar_png');
            image.x = 608;
            image.y = 1124;
            group.addChild(image);
            // 底图
            var image = createImg('pic_base01_sidebar_png', '26,28,26,28',
                { width: 604, height: 540 });
            image.x = gWidth / 2;
            image.y = 788;
            group.addChild(image);
            // 列表0
            var list = createList(more.SliderItem0, config.list0, 560, 494, { spacingY: 12 });
            list.x = gWidth / 2;
            list.y = 787;
            group.addChild(list);
            // 
            var image = createImg('pic_base02_sidebar_png', '0,32,0,200', { width: 604 });
            image.x = gWidth / 2;
            image.y = 349;
            group.addChild(image);
            // 小猪
            var image = createImg('pic_pig_sidebar_png');
            image.x = 70;
            image.y = 222;
            group.addChild(image);
            // 列表1
            var list = createList(more.SliderItem1, config.list1, 496, 260, {
                spacingX: 5, spacingY: 5
            });
            list.x = 382;
            list.y = 364;
            group.addChild(list);
            return _this;
        }
        /**
         * 添加关闭回调
         */
        SliderList.prototype.addClose = function (call, thisObj) {
            var self = this;
            var clzz = more.MoreUtils;
            clzz.addTouchEndListener(self.m_rtBg, call, thisObj);
            clzz.addTouchEndScaleListener(self.m_imgClose, call, thisObj);
        };
        return SliderList;
    } (cc.Node));
    more.SliderList = SliderList;
})(more || (more = {}));

//// SliderUtils ////
var more;
(function (more) {
    /**
     * 滑块列表工具
     */
    var SliderUtils = /** @class */ (function () {
        function SliderUtils() {
        }
        /**
         * 初始化配置
         */
        SliderUtils.initConfig = function (url) {
            var self = SliderUtils;
            var repeate = new more.RepeatUtils(1000, 5);
            var call = function () {
                return new Promise(function (resolve) {
                    more.MoreUtils.getResByUrl(url).
                        then(function (data) {
                            self.config = data;
                            resolve(true);
                        }).catch(function () {
                            resolve(false);
                        });
                });
            };
            repeate.setRepeatCall(call);
            return new Promise(function (resolve, reject) {
                repeate.setFinishCall(function (bool) {
                    bool ? resolve() : reject();
                });
            });
        };
        /**
         * 初始化文件
         */
        SliderUtils.initFile = function () {
            return __awaiter(this, void 0, void 0, function () {
                var self, url, data, png, sheet, frames, create, spFrames, spFrame, i, frame;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            self = SliderUtils;
                            url = self.sheetUrl;
                            return [4 /*yield*/, more.MoreUtils.getResByUrl(url)];
                        case 1:
                            data = _a.sent();
                            png = url.substr(0, url.lastIndexOf('/') + 1) + data.file;
                            return [4 /*yield*/, more.MoreUtils.getResByUrl(png)];
                        case 2:
                            sheet = _a.sent();
                            frames = data.frames;
                            spFrames = self.$spFrames = {};
                            for (i in frames) {
                                frame = frames[i];
                                spFrame = spFrames[i] = new cc.SpriteFrame();
                                spFrame.setTexture(sheet, cc.rect(frame.x, frame.y, frame.sourceW, frame.sourceH));
                                spFrame.setOriginalSize(cc.size(frame.w, frame.h));
                            }
                            return [2 /*return*/];
                    }
                });
            });
        };
        /**
         * 设置图片，会添加属性width和height
         * @param img 图片
         * @param name 合图里的子图别称，如'xxx_png'
         */
        SliderUtils.setImage = function (img, name) {
            var frame = SliderUtils.$spFrames[name];
            if (frame) {
                var rect = frame._rect;
                img.spriteFrame = frame;
                img.width = rect.width;
                img.height = rect.height;
            }
        };
        /**
         * 创建滑块，默认贴着父控件左边，Y值可修改
         * @param parent 滑块存放位置，建议高度等同于屏幕高，可使用addChild功能
         * @param y 滑块的Y值，不传则默认0.6986位置
         */
        SliderUtils.createSliderBar = function (parent, y) {
            return __awaiter(this, void 0, void 0, function () {
                var self, slide, game, enable;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            self = SliderUtils;
                            return [4 /*yield*/, self.initFile()];
                        case 1:
                            _a.sent();
                            if (y == void 0)
                                y = parent.height * 0.6986;
                            slide = new cc.Node;
                            slide.x = 15;
                            slide.y = y;
                            parent.addChild(slide);
                            // 添加精灵
                            self.setImage(slide.addComponent(cc.Sprite), 'icon_sidebar_png');
                            // 点击滑块
                            more.MoreUtils.addTouchEndListener(slide, function () {
                                var tween = more.Tween;
                                // 添加列表
                                var list = new more.SliderList;
                                var time = 120;
                                list.x = -750;
                                parent.addChild(list);
                                // 出场
                                tween.get(slide).to({ x: -77 }, time);
                                tween.get(list).to({ x: 0 }, time).call(enable, null, true);
                                // 关闭
                                list.addClose(function () {
                                    tween.get(slide).to({ x: 15 }, time);
                                    tween.get(list).to({ x: -750 }, time).call(function () {
                                        more.MoreUtils.removeEventListeners(list);
                                        parent.removeChild(list);
                                    });
                                });
                            });
                            return [2 /*return*/];
                    }
                });
            });
        };
        /**
         * 合图地址
         */
        SliderUtils.sheetUrl = 'https://static.xunguanggame.com/moreGame/sliderbar.json';
        return SliderUtils;
    } ());
    more.SliderUtils = SliderUtils;
})(more || (more = {}));

// 重写清理
var director = cc.director;
var func = director._loadSceneByUuid;
director._loadSceneByUuid = function () {
    var scene = director.getScene();
    clearLogo(scene);
    func.apply(this, arguments);
};

/**
 * 清除Logo
 */
var clearLogo = more.clearLogo = function (node) {
    if (node) {
        var cs = node.children;
        for (let i in cs)
            more.clearLogo(cs[i]);
        if (node instanceof more.MoreLogo) {
            node.clear();
            node.destroy();
        }
    }
};

/**
 * 延迟一帧
 */
var later = function (func) {
    setTimeout(func, 20);
};

window.more = more;

/* Test
more.MoreUtils.setConfig('https://static.xunguanggame.com/moreGame/sdks/model.json').then(function (config) {
    var scene = cc.director.getScene();
    var stage = new cc.Node;
    stage.setContentSize(750, 1334);
    stage.scale = .4;
    scene.addChild(stage);
    // 单个图标
    var logo = new more.MoreLogo();
    stage.addChild(logo);
    logo.x = 100;
    logo.y = 800;
    // 左边滑块-列表示例
    more.SliderUtils.createSliderBar(stage);
    // 底部Banner，类型为1，正确应由广告播放失败调用
    var banner = new more.MoreLogo(1);
    stage.addChild(banner);
    banner.x = 375;
    banner.y = banner.height / 2;
    banner.anchorY = 0;
});
*/