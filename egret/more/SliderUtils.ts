module more {

	/**
	 * 滑块列表工具
	 */
	export class SliderUtils {

		/**
		 * 合图地址
		 */
		public static sheetUrl: string = 'https://static.xunguanggame.com/moreGame/sliderbar.json';

		/**
		 * 初始化文件
		 */
		protected static initFile(): Promise<any> {
			return RES.getResByUrl(SliderUtils.sheetUrl, void 0, void 0,
				RES.ResourceItem.TYPE_SHEET);
		}

		/**
		 * 设置图片
		 * @param img 图片
		 * @param name 合图里的子图别称，如'xxx_png'
		 */
		public static setImage(img: eui.Image, name: string): void {
			img.source = SliderUtils.sheetUrl + '#' + name;
		}

		/**
		 * 创建滑块，默认贴着父控件左边，Y值可修改
		 * @param parent 滑块存放位置，建议高度等同于屏幕高
		 * @param y 滑块的Y值，不传则默认0.2624位置
		 */
		public static async createSliderBar(parent: egret.DisplayObjectContainer, y?: number): Promise<void> {
			var self = SliderUtils;
			await self.initFile();
			if (y == void 0)
				y = parent.height * 0.2624;
			var slide = new eui.Image;
			slide.x = -62;
			slide.y = y;
			parent.addChild(slide);
			self.setImage(slide, 'icon_sidebar_png');
			// 监听
			var stage = egret.sys.$TempStage;
			var enable = function (bool) {
				stage.touchChildren = bool;
			};
			// 点击滑块
			MoreUtils.addTouchTapListener(slide, function () {
				let tween = egret.Tween;
				// 添加列表
				let list = new SliderList;
				let time = 120;
				list.x = -750;
				parent.addChild(list);
				// 出场
				enable(false);
				tween.get(slide).to({ x: -slide.width }, time);
				tween.get(list).to({ x: 0 }, time).call(enable, null, [true]);
				// 关闭
				list.addClose(function () {
					enable(false);
					tween.get(slide).to({ x: -62 }, time);
					tween.get(list).to({ x: -750 }, time).call(function () {
						enable(true);
						parent.removeChild(list);
					});
				});
			});
		}
	}
}