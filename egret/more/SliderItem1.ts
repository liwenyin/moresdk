module more {

	/**
	 * 子项数据
	 */
	interface ISliderData1 extends IMiniConfig {
		reddot: boolean;		// 显示红点
	}

	/**
	 * 滑块列表子项1
	 */
	export class SliderItem1 extends eui.ItemRenderer {

		private m_imgLogo: eui.Image;
		private m_imgDot: eui.Image;

		public data: ISliderData1;

		public constructor() {
			super();
			var set = SliderUtils.setImage;
			this.width = this.height = 120;
			// logo
			var image = this.m_imgLogo = new eui.Image;
			image.y = 10;
			image.width = image.height = 110;
			this.addChild(image);
			// 遮罩
			var image = new eui.Image;
			set(image, 'game_mask_png');
			image.y = 10;
			image.width = image.height = 110;
			this.addChild(image);
			// 红点
			var image = this.m_imgDot = new eui.Image;
			set(image, 'pic_new_sidebar_png');
			image.x = 96;
			this.addChild(image);
			// 监听
			MoreUtils.addTouchTapListener(this, this.onClick, this);
		}

		/**
		 * 点击
		 */
		protected onClick(): void {
			var config = this.data;
			MoreUtils.toMiniProgram(config.appId, config.path, config.preImg);
		}

		protected dataChanged(): void {
			var self = this;
			var data = self.data;
			self.m_imgLogo.source = data.logo;
			self.m_imgDot.visible = data.reddot;
		}
	}
}