module more {

	/**
	 * 更多游戏——单个logo
	 * 注：该logo的锚点位于中心点，因此加入场景时注意坐标
	 */
	export class MoreLogo extends eui.Image {

		private $initAc: boolean;
		private $interval: number;
		private $movie: number;
		private $index: number;
		private $games: IMiniConfig[];

		/**
		 * @param type 图标类型，默认games配置
		 */
		public constructor(type?: number) {
			super();
			this.initView(MoreUtils.config, type);
		}

		/**
		 * 重写
		 */
		public $setTexture(texture: egret.Texture): boolean {
			var self = this;
			if (!self.$initAc) {
				let width = 0, height = 0;
				if (texture) {
					width = texture.textureWidth;
					height = texture.textureHeight;
				}
				self.setCAnchor(width, height);
			}
			return super.$setTexture(texture);
		}

		/**
		 * 初始化界面
		 */
		protected initView(config: IMoreConfig, type?: number): void {
			var self = this;
			// 显示Logo
			var isBanner = type == 1;
			var games = self.$games = config[isBanner ? 'banner' : 'games'];
			var length = games && games.length;
			if (length > 0) {
				let clzz = MoreUtils;
				// 先刷一次
				self.$index = -1;
				self.updateIndex();
				// 轮询
				if (length > 1) {
					let pollTime = config.pollTime;
					if (!(pollTime > 0))
						pollTime = 30000;
					self.$interval = egret.setInterval(self.updateIndex, self, pollTime);
				}
				// 其他
				isBanner || clzz.swing(self);
				clzz.addTouchTapListener(self, self.onClick, self);
			}
			else
				self.source = '';
		}

		/**
		 * 更新logo下标
		 */
		protected updateIndex(): void {
			var self = this;
			var games = self.$games;
			var index = self.$index = (self.$index + 1) % games.length;
			var game = games[index], logo = game.logo;
			// 初始化
			self.$initAc = false;
			self.clearMovie();
			// 尺寸
			var size = game.size;
			if (size) {
				self.setCAnchor(self.width = size.w,
					self.height = size.h);
				self.$initAc = true;
			}
			else {
				self.width = self.height = void 0;
			}
			// 动画
			if (MoreUtils.checkFile(logo, '.json'))
				self.initMovie(logo.substr(0, logo.length - 5));
			else
				self.source = logo;
		}

		/**
		 * 设置中心锚点，即宽高的一半
		 */
		public setCAnchor(width: number, height: number): void {
			var self = this;
			self.anchorOffsetX = width / 2;
			self.anchorOffsetY = height / 2;
		}

		/**
		 * 初始化动画
		 */
		private async initMovie(name: string): Promise<void> {
			var getRes = RES.getResByUrl;
			var pngUrl = name + '.png';
			var frame = await getRes(name + '.json');
			var sheet = await getRes(pngUrl);
			var factory = new egret.MovieClipDataFactory(frame, sheet);
			var data = factory.generateMovieClipData();
			if (data) {
				let self = this, index = 0;
				let frames = data.frames;
				let fSize = frames.length;
				let speed = 1000 / (data.frameRate || 5);
				let call = function () {
					// 渣渣还减1..
					self.texture = data.getTextureByFrame(++index);
					index %= fSize;
				};
				// 动态切图
				call();
				self.$movie = egret.setInterval(call, null, speed);
			}
		}

		/**
		 * 清除动画定时器
		 */
		private clearMovie(): void {
			var self = this;
			if (self.$movie) {
				egret.clearInterval(self.$movie);
				self.$movie = null;
			}
			self.source = '';
		}

		/**
		 * 点击
		 */
		protected onClick(): void {
			var config = this.$games[this.$index];
			config && MoreUtils.toMiniProgram(
				config.appId, config.path, config.preImg);
		}

		/**
		 * 清除事件、动画
		 */
		public clear(): void {
			var self = this;
			self.source = '';
			egret.clearInterval(self.$interval);
			egret.Tween.removeTweens(self);
			MoreUtils.removeEventListener(self);
		}

		/**
		 * 重写
		 */
		public $onRemoveFromStage(): void {
			super.$onRemoveFromeStage();
			this.clear();
		}
	}
}