module more {

	/**
	 * 滑块列表
	 */
	export class SliderList extends eui.Component {

		private m_rtBg: eui.Rect;
		private m_imgClose: eui.Image;
		private m_ltGame0: eui.List;
		private m_ltGame1: eui.List;

		public constructor() {
			super();
			this.width = 750;
			this.height = AppViews.stageHeight;
			// 透明背景
			var rect = this.m_rtBg = new eui.Rect;
			rect.alpha = .4;
			rect.percentWidth = 100;
			rect.percentHeight = 100;
			this.addChild(rect);
			// 内容
			var group = new eui.Group;
			group.width = 666;
			group.height = 1178;
			group.horizontalCenter = group.verticalCenter = 0;
			this.addChild(group);
			// 背景
			var set = SliderUtils.setImage;
			var image = new eui.Image;
			image.horizontalCenter = 0;
			image.height = 1020;
			image.scale9Grid = new egret.Rectangle(110, 100, 473, 29);
			set(image, 'pic_popbg_sidebar_png');
			group.addChild(image);
			// 关闭按钮
			var image = this.m_imgClose = new eui.Image;
			set(image, 'btn_close_sidebar_png');
			image.x = 608;
			image.y = 54;
			image.anchorOffsetX = 38;
			image.anchorOffsetY = 32;
			group.addChild(image);
			// 底图
			var image = new eui.Image;
			set(image, 'pic_base01_sidebar_png');
			image.y = 120;
			image.horizontalCenter = 0;
			image.width = 604;
			image.height = 540;
			image.scale9Grid = new egret.Rectangle(27, 27, 6, 7);
			group.addChild(image);
			// 列表0
			var scroller = new eui.Scroller;
			var list = scroller.viewport = this.m_ltGame0 = new eui.List;
			scroller.width = 560;
			scroller.height = 494;
			scroller.horizontalCenter = 0;
			scroller.y = 144;
			(list.layout = new eui.VerticalLayout).gap = 12;
			group.addChild(scroller);
			// 
			var image = new eui.Image;
			set(image, 'pic_base02_sidebar_png');
			image.horizontalCenter = 0;
			image.y = 676;
			image.width = 604;
			image.scale9Grid = new egret.Rectangle(184, 38, 18, 228);
			group.addChild(image);
			// 小猪
			var image = new eui.Image;
			set(image, 'pic_pig_sidebar_png');
			image.y = 907;
			group.addChild(image);
			// 列表1
			var scroller = new eui.Scroller;
			var list = scroller.viewport = this.m_ltGame1 = new eui.List;
			var layout = list.layout = new eui.TileLayout;
			scroller.width = 495;
			scroller.height = 260;
			scroller.x = 134;
			scroller.y = 684;
			layout.horizontalGap = layout.verticalGap = 5;
			layout.requestedColumnCount = 4;
			group.addChild(scroller);
		}

		protected childrenCreated(): void {
			var self = this;
			var list0 = self.m_ltGame0;
			var list1 = self.m_ltGame1;
			var config = MoreUtils.config;
			list0.itemRenderer = SliderItem0;
			list1.itemRenderer = SliderItem1;
			list0.dataProvider = new eui.ArrayCollection(config.list0);
			list1.dataProvider = new eui.ArrayCollection(config.list1);
		}

		public $onRemoveFromStage(): void {
			super.$onRemoveFromStage();
			var self = this;
			var utils = MoreUtils;
			var remove = utils.removeListData;
			utils.removeEventListeners(self);
			remove(self.m_ltGame0, true);
			remove(self.m_ltGame1, true);
		}

		/**
		 * 添加关闭回调
		 */
		public addClose(call: Function, thisObj?: any): void {
			var self = this;
			var clzz = MoreUtils;
			clzz.addTouchTapListener(self.m_rtBg, call, thisObj);
			clzz.addTouchTapScaleListener(self.m_imgClose, call, thisObj);
		}
	}
}