module more {

	/**
	 * 根据运行次数返回Promise对象
	 */
	type TRepeatCall = (count?: number) => Promise<boolean>;

	/**
	 * 重复执行某一操作工具类，一般用于定时发起网络请求用。一次性工具，用完不复用
	 * 定时：上一次操作完成时和下一次操作开始时的时间间隔，而非两个操作开始时的时间间隔；
	 * 功能：定时发起重复操作回调，当回调结果为true时结束回调，并通知成功；
	 * 当在规定上限内还未收到true的话也会结束回调，并通知失败
	 */
	export class RepeatUtils {

		private $timer: egret.Timer;			// 定时器
		private $startTime: number;				// 上次启动时间
		private $maxCount: number;				// 最大运行次数
		private $call: TRepeatCall;				// 操作函数
		private $thisObj: any;					// 操作函数所属对象
		private $fCall: (bool: boolean) => void;// 结束函数
		private $fThisObj: any;					// 结束函数所属对象

		protected isRunning: boolean;			// 是否运行中
		protected repeatTime: number;			// 重复时间间隔，毫秒数
		protected repeatCount: number;			// 当前实时剩余次数，小于等于0会结束回调

		/**
		 * @param repeatTime 单位毫秒，即每隔多久发起一次操作
		 * @param repeatCount 操作次数上限，默认一次
		 */
		public constructor(repeatTime: number, repeatCount: number = 1) {
			this.repeatTime = repeatTime
			this.$maxCount = this.repeatCount = isNaN(repeatCount) ? 0 : repeatCount;
		}

		/**
		 * 定时器回调
		 */
		protected onTimer(): void {
			var self = this;
			var curT = Date.now();
			if (curT - self.$startTime >= self.repeatTime && !self.isRunning) {
				let endCall = function () {
					self.isRunning = false;
					if (!(--self.repeatCount > 0)) {
						self.onFinish(false);
					}
				};
				self.$startTime = curT;
				self.isRunning = true;
				// 创建promise
				let promise = self.$call.call(self.$thisObj, self.$maxCount - self.repeatCount);
				if (promise instanceof Promise) {
					promise.then(function (bool) {
						bool ? self.onFinish(true) : endCall();
					}).catch(endCall);
				}
				else {
					endCall();
				}
			}
		}

		/**
		 * 回调结束
		 * @param bool 操作结果
		 */
		protected onFinish(bool: boolean): void {
			var self = this;
			var timer = self.$timer;
			self.isRunning = false;
			if (timer) {
				let fCall = self.$fCall;
				fCall && fCall.call(self.$fThisObj, bool);;
				MoreUtils.removeEventListener(timer);
				self.$call = self.$thisObj = self.$fCall = self.$fThisObj = self.$timer = null;
			}
		}

		/**
		 * 设置结束回调，不设置也不会怎么样
		 * @param call 参数表示是否正常结束
		 */
		public setFinishCall(call: (bool: boolean) => void, thisObj?: any): void {
			var self = this;
			self.$fCall = call;
			self.$fThisObj = thisObj;
		}

		/**
		 * 设置重复回调，并自动开始。建议不要重复设置
		 * @param call 根据运行次数返回Promise对象
		 */
		public setRepeatCall(call: TRepeatCall, thisObj?: any): void {
			var self = this;
			if (!self.$timer && self.$call !== call) {
				self.$call = call;
				self.$thisObj = thisObj;
				if (call && self.repeatCount > 0) {
					let timer = self.$timer = new egret.Timer(0, self.repeatTime);
					timer.addEventListener(egret.TimerEvent.TIMER, self.onTimer, self);
					self.$startTime = 0;
					self.onTimer();		// 先执行一次
					timer.start();
				}
			}
		}
	}
}