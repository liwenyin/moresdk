module more {

	declare var wx;

	/**
	 * 监听对象
	 */
	interface TargetEvent {
		type: string;
		listener: Function;
		thisObject: any;
		useCapture: boolean;
		priority: number;
	}

	interface ScaleTarget extends egret.DisplayObject {
		$hashBegin?: boolean;// 是否按下
		$bgScaleX?: number;	// 初始缩放值
		$bgScaleY?: number;
		$evtScale?: number;	// 缩放倍数
	}

	/**
	 * 小程序配置
	 */
	export interface IMiniConfig {
		logo: string;		// 显示的图标
		name: string;		// 游戏名称
		appId: string;		// 游戏ID
		path: string;		// 跳转路径
		preImg: string;		// 无法跳转时弹出图片
		size: { w: number, h: number };	// 设置图标尺寸
	}

	/**
	 * 更多游戏配置
	 */
	export interface IMoreConfig {
		games: IMiniConfig[];	// logo
		list0: IMiniConfig[];	// 列表0
		list1: IMiniConfig[];	// 列表1
		banner: IMiniConfig[];	// 底部
		pollTime: number;		// 轮询时间，默认30秒
		updateTime: number;		// 刷新配置时间
	}

	/**
	 * 更多游戏工具类，微信交接工具
	 */
	export class MoreUtils {

		/**
		 * SDK配置
		 */
		public static config: IMoreConfig;

		/**
		 * 循环左右摇晃动画，注意控件必须锚点居中或者有水平垂直约束，否则会很怪异
		 * @param time 第一段动画时间
		 */
		public static swing(target: egret.DisplayObject, time: number = 100): void {
			egret.Tween.get(target, { loop: true }).to({
				rotation: 30
			}, time).to({
				rotation: -30
			}, time * 1.6).to({
				rotation: 15
			}, time * 1.2).to({
				rotation: -15
			}, time * .8).to({
				rotation: 0
			}, time * .6).wait(time * 8);
		}

		//// 监听相关 ////

		/**
		 * 移除控件上的所有监听，该方法也适用于没有通过addEventListener来添加的控件
		 */
		public static removeEventListener(target: egret.EventDispatcher): void {
			var value = target.$EventDispatcher;
			var list = [].concat(value[1] || [], value[2] || []);
			for (let i in list) {
				let item = list[i];
				for (let j in item) {
					let datas = <TargetEvent[]>item[j];
					for (let k in datas) {
						let event = datas[k];
						target.removeEventListener(event.type, event.listener, event.thisObject, event.useCapture);
					}
				}
			}
		}

		/**
		 * 移除root往下所有的点击事件
		 */
		public static removeEventListeners(root: egret.EventDispatcher): void {
			var self = MoreUtils;
			if (root instanceof egret.DisplayObjectContainer)
				for (let i = 0, num = root.numChildren; i < num; i++)
					self.removeEventListeners(root.getChildAt(i));
			else
				self.removeEventListener(root);
		}

		/**
		 * 添加点击按下监听
		 */
		public static addTouchBeginListener(target: egret.EventDispatcher, call: Function, thisObj?: any): void {
			target.addEventListener(egret.TouchEvent.TOUCH_BEGIN, call, thisObj);
		}

		/**
		 * 添加监听结束监听
		 */
		public static addTouchFinishListener(target: egret.EventDispatcher, finish: Function, thisObj?: any): void {
			var event = egret.TouchEvent;
			target.addEventListener(event.TOUCH_END, finish, thisObj);
			target.addEventListener(event.TOUCH_CANCEL, finish, thisObj);
			target.addEventListener(event.TOUCH_RELEASE_OUTSIDE, finish, thisObj);
		}

		/**
		 * 添加TouchTap监听
		 */
		public static addTouchTapListener(target: egret.DisplayObject, call: Function, thisObj?: any, useCapture?: boolean): void {
			target.addEventListener(egret.TouchEvent.TOUCH_TAP, call, thisObj, useCapture);
		}

		/**
		 * 添加缩放监听，记得用removeEventListener来移除这个监听
		 */
		public static addScaleListener(target: ScaleTarget, scale: number = 0.95): void {
			var self = MoreUtils;
			target.$evtScale = scale;
			target.$bgScaleX = target.scaleX;
			target.$bgScaleY = target.scaleY;
			self.addTouchBeginListener(target, self.onScaleBegin, self);
			self.addTouchFinishListener(target, self.onScaleEnd, self);
		}

		/**
		 * 缩放开始
		 */
		protected static onScaleBegin(event: egret.TouchEvent): void {
			var target = <ScaleTarget>event.currentTarget;
			var tween = egret.Tween;
			var scale = target.$evtScale;
			var scaleX = target.scaleX * scale;
			var scaleY = target.scaleY * scale;
			target.$hashBegin = true;
			tween.removeTweens(target);
			tween.get(target).to({ scaleX: scaleX, scaleY: scaleY }, 100);
		}

		/**
		 * 缩放结束
		 */
		protected static onScaleEnd(event: egret.TouchEvent): void {
			var target = <ScaleTarget>event.currentTarget;
			var time = 100;
			var tween = egret.Tween;
			var scaleX = target.$bgScaleX;
			var scaleY = target.$bgScaleY;
			var bScaleX = scaleX * 1.1;
			var bScaleY = scaleY * 1.1;
			tween.removeTweens(target);
			target.$hashBegin && tween.get(target).to({ scaleX: bScaleX, scaleY: bScaleY }, time).to({ scaleX: scaleX, scaleY: scaleY }, time);
			target.$hashBegin = void 0;
		}

		/**
		 * 在TouchTap的基础上进行缩放
		 */
		public static addTouchTapScaleListener(target: ScaleTarget, call: Function, thisObj?: any, scale?: number, useCapture?: boolean): void {
			var self = MoreUtils;
			self.addScaleListener(target, scale);
			self.addTouchTapListener(target, call, thisObj, useCapture);
		}

		//// 监听结束 ////

		/**
		 * 跳转到小程序
		 * @param appId 小程序的appId
		 * @param path 跳转路径
		 * @param extraData 扩展消息
		 * @param preImg 低版本弹出图片
		 */
		public static toMiniProgram(appId: string, path?: string, preImg?: string): Promise<boolean> {
			var toMini = wx.navigateToMiniProgram;
			if (toMini) {
				return new Promise<boolean>(function (resolve) {
					toMini({
						appId: appId,
						path: path,
						envVersion: 'trial',
						// extraData: extraData,
						success: function (res) {
							resolve(true);
						},
						fail: function (res) {
							MoreUtils.previewImage(preImg).then(resolve);
						}
					});
				});
			}
			else
				return MoreUtils.previewImage(preImg);
		}

		/**
		 * 显示图片
		 */
		public static previewImage(url: string): Promise<boolean> {
			return new Promise<boolean>(function (resolve) {
				url ? wx.previewImage({
					urls: [url],
					success: function () {
						resolve(true);
					},
					fail: function () {
						resolve(false);
					}
				}) : resolve(false);
			})
		}

		/**
		 * 检测文件格式
		 * @param url 文件地址
		 * @param format 格式，建议首字符为'.'，如'.png'、'.json'等
		 */
		public static checkFile(url: string, format: string): boolean {
			return url.indexOf(format) == url.length - format.length;
		}

		/**
         * 移除List的数据
         * @param list
         * @param isCache 是否是缓存数据，是的话不清除
         */
		public static removeListData(list: eui.List, isCache?: boolean): void {
			var datas = <eui.ArrayCollection>list.dataProvider;
			datas && !isCache && datas.removeAll();
			list.dataProvider = null;
		}

		/**
		 * 设置SDK配置，需要优先调用
		 */
		public static setConfig(url: string): Promise<IMoreConfig> {
			var self = MoreUtils;
			var repeate = new RepeatUtils(1000, 5);
			var call = function () {
				return new Promise<boolean>(function (resolve) {
					RES.getResByUrl(url).then(function (data: IMoreConfig) {
						// 刷新配置操作
						let uTime = data.updateTime;
						uTime > 0 && egret.setTimeout(self.setConfig, self, 3000, url);
						// 返回
						self.config = data;
						resolve(true);
					}).catch(function () {
						resolve(false);
					});
				});
			};
			repeate.setRepeatCall(call);
			return new Promise<IMoreConfig>(function (resolve, reject) {
				repeate.setFinishCall(function (bool: boolean) {
					bool ? resolve(self.config) : reject();
				});
			});
		}
	}
}