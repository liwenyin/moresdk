module more {

	/**
	 * 子项数据
	 */
	interface ISliderData0 extends IMiniConfig {
		number: number;		// 钻石数量
	}

	/**
	 * 滑块列表子项0
	 */
	export class SliderItem0 extends eui.ItemRenderer {

		private m_imgLogo: eui.Image;
		private m_lblName: eui.Label;
		private m_lblNum: eui.Label;
		private m_imgRec: eui.Image;

		public data: ISliderData0;

		public constructor() {
			super();
			this.width = 560;
			this.height = 134;
			var set = SliderUtils.setImage;
			var image = new eui.Image;
			set(image, 'pic_grade_sidebar_png');
			image.scale9Grid = new egret.Rectangle(73, 59, 24, 19);
			image.percentWidth = 100;
			this.addChild(image);
			// logo
			var image = this.m_imgLogo = new eui.Image;
			image.x = 20;
			image.y = 12;
			image.width = image.height = 110;
			this.addChild(image);
			// 遮罩
			var image = new eui.Image;
			set(image, 'game_mask_png');
			image.x = 20;
			image.y = 12;
			image.width = image.height = 110;
			this.addChild(image);
			// 名称
			var label = this.m_lblName = new eui.Label;
			label.x = 162;
			label.y = 30;
			label.size = 28;
			label.textColor = 0x261009;
			this.addChild(label);
			// 
			var image = new eui.Image;
			set(image, 'pic_count_sidebar_png');
			image.x = 156;
			image.y = 68;
			this.addChild(image);
			// 数量
			var label = this.m_lblNum = new eui.Label;
			label.horizontalCenter = -30;
			label.verticalCenter = 23;
			label.size = 28;
			label.textColor = 0xa74f07;
			this.addChild(label);
			// 按钮
			var image = this.m_imgRec = new eui.Image;
			set(image, 'btn_get_sidebar_png');
			image.horizontalCenter = 173;
			image.verticalCenter = 0;
			this.addChild(image);
		}

		protected childrenCreated(): void {
			var self = this;
			MoreUtils.addTouchTapScaleListener(self.m_imgRec, self.onClick, self);
		}

		/**
		 * 点击
		 */
		protected onClick(): void {
			var config = this.data;
			MoreUtils.toMiniProgram(config.appId, config.path, config.preImg);
		}

		protected dataChanged(): void {
			var self = this;
			var data = self.data;
			self.m_imgLogo.source = data.logo;
			self.m_lblName.text = data.name;
			self.m_lblNum.text = (data.number || 500) + '';
		}
	}
}