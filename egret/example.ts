// SDK示例：设置配置，若存在，则执行SDK界面绘制
more.MoreUtils.setConfig('https://static.xunguanggame.com/moreGame/sdks/model.json').then(function (config) {
    // 使用层级管理示例1，则不直接加入舞台
    // class MoreUI extends lie.UIComponent {
    //     constructor() {
    //         super();
    //         var stage = AppViews.stage;
    //         this.width = stage.stageWidth;
    //         this.height = stage.stageHeight;
    //     }
    // }
    // var stage = AppViews.pushTop(MoreUI);
    // 为使用层级管理，示例2
    var stage = egret.sys.$TempStage;
    // 单个图标
    var logo = new more.MoreLogo();
    stage.addChild(logo);
    logo.x = 640;
    logo.y = 820 + pfUtils.offsetY;
    // 左边滑块-列表示例x
    more.SliderUtils.createSliderBar(stage);
    // 底部Banner，类型为1，由广告显示失败时调用
    var banner = new more.MoreLogo(1);
    stage.addChild(banner);
    banner.x = 375; // banner.horizontalCenter = 0;
    banner.bottom = 0;

    // 广告正确使用示例：
    // var banner: more.MoreLogo;
    // var create = function () {
    //     if (!banner) {
    //         banner = new more.MoreLogo(1);
    //         stage.addChild(banner);
    //         banner.x = 375; // banner.horizontalCenter = 0;
    //         banner.bottom = 0;
    //     }
    // };
    // var remove = function () {
    //     if (banner) {
    //         stage.removeChild(banner);
    //         banner = null;
    //     }
    // };
    // var register = Dispatch.register;
    // 两个事件分别在广告对象的onLoad和onError响应
    // register(MsgStr.MS_ErrBanner, create);
    // register(MsgStr.MS_ShowBanner, remove);
});